<?php

/*

*Plugin Name: kb-plugin

*Plugin URI: http://www.ibrinfotech.com/

*Description: plugin on client demand Sortcode using Affilite form [Affiliates]

*Version: 1.1

*Author: Ibr Infotech

*Author URI: http://www.ibrinfotech.com/author/admin/ 

*/


global $custom_table_kb_plugin_db_version;

$custom_table_kb_plugin_db_version = '1.1'; // version changed from 1.0 to 1.1

define( 'PLUGIN_DIR', dirname(__FILE__).'/' ); 



function custom_table_kb_plugin_install()

{

    global $wpdb;

    global $custom_table_kb_plugin_db_version;



    $table_name = $wpdb->prefix .'kb_affiliated'; // do not forget about tables prefix
 

	$sql = "CREATE TABLE " . $table_name . " (

  `af_id` int(100) NOT NULL AUTO_INCREMENT,

  `af_fname` varchar(100) DEFAULT NULL,

  `af_lname` varchar(100) DEFAULT NULL,

  `af_email` varchar(100) DEFAULT NULL,

  `af_dob` date DEFAULT NULL,

  `af_address` varchar(100) DEFAULT NULL,

  `af_city` varchar(100) DEFAULT NULL,

  `af_state` varchar(100) DEFAULT NULL,

  `af_zipcode` varchar(100) DEFAULT NULL,

  `af_country` varchar(100) DEFAULT NULL,

  `af_mo_no` bigint(100) NOT NULL,

  `kb_infoid` int(10) DEFAULT NULL,

  `kb_af_id` int(111) NOT NULL,

  `kb_username` varchar(222) NOT NULL,

  `kb_password` varchar(222) NOT NULL,

  `kb_team_side` varchar(222) NOT NULL,

  `kb_id_proof` varchar(222) NOT NULL,

  `kb_add_proof` varchar(222) NOT NULL,

  `kb_package` varchar(222) NOT NULL,

  `kb_sponsor` varchar(222) NOT NULL,

  `kb_signup_by` int(222) NOT NULL DEFAULT '0',

  `kb_acc_created` varchar(222) NOT NULL,

  `kb_auto_payment` varchar(222) NOT NULL,

  `af_add_date` datetime DEFAULT NULL,

  PRIMARY KEY (`af_id`)

	);";

    // we do not execute sql directly

    // we are calling dbDelta which cant migrate database

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    dbDelta($sql);

    add_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);


    $installed_ver = get_option('custom_table_kb_plugin_db_version');

    if ($installed_ver != $custom_table_kb_plugin_db_version) {

        $sql = "CREATE TABLE " . $table_name . " (

  `af_id` int(100) NOT NULL AUTO_INCREMENT,

  `af_fname` varchar(100) DEFAULT NULL,

  `af_lname` varchar(100) DEFAULT NULL,

  `af_email` varchar(100) DEFAULT NULL,

  `af_dob` date DEFAULT NULL,

  `af_address` varchar(100) DEFAULT NULL,

  `af_city` varchar(100) DEFAULT NULL,

  `af_state` varchar(100) DEFAULT NULL,

  `af_zipcode` varchar(100) DEFAULT NULL,

  `af_country` varchar(100) DEFAULT NULL,

  `af_mo_no` bigint(100) NOT NULL,

  `kb_infoid` int(10) DEFAULT NULL,

  `kb_af_id` int(111) NOT NULL,

  `kb_username` varchar(222) NOT NULL,

  `kb_password` varchar(222) NOT NULL,

  `kb_team_side` varchar(222) NOT NULL,

  `kb_id_proof` varchar(222) NOT NULL,

  `kb_add_proof` varchar(222) NOT NULL,

  `kb_package` varchar(222) NOT NULL,

  `kb_sponsor` varchar(222) NOT NULL,

  `kb_signup_by` varchar(222) NOT NULL,

  `kb_acc_created` varchar(222) NOT NULL,

  `kb_auto_payment` varchar(222) NOT NULL,

  `af_add_date` datetime DEFAULT NULL,

  PRIMARY KEY (`af_id`)

        );";



        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        dbDelta($sql);



        // notice that we are updating option, rather than adding it

        update_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);

    }

}



register_activation_hook(__FILE__, 'custom_table_kb_plugin_install');



///////////////////////////2222222222222////////////////////////////

function custom_table_kb_plugin_kb_sequence_step()

{

    global $wpdb;

    global $custom_table_kb_plugin_db_version;



    $table_name = $wpdb->prefix .'kb_sequence_step'; // do not forget about tables prefix

	$sql = "CREATE TABLE " . $table_name . " (

  `seq_step_id` int(111) NOT NULL AUTO_INCREMENT,

  `seq_step_subject` varchar(200) NOT NULL,

  `seq_step_wait` varchar(200) NOT NULL,

  `seq_step_Delay_Times` varchar(123) NOT NULL,

  `seq_step_delaytime` varchar(200) NOT NULL,

  `seq_step_weekday` varchar(200) NOT NULL,

  `seq_step_type` varchar(200) NOT NULL,

  `seq_step_content` text NOT NULL,

  `seq_step_adddate` varchar(200) NOT NULL,

  `seq_step_email_email` varchar(200) NOT NULL,

  `seq_step_email_send_name` varchar(200) NOT NULL,

  `seq_step_email_rplay_email` varchar(200) NOT NULL,

  `seq_step_email_email_from` varchar(200) NOT NULL,

  `seq_step_tasks_due_date` varchar(200) NOT NULL,

  `seq_step_tasks_onwer` varchar(200) NOT NULL,

  `seq_step_tasks_onwer_type` varchar(111) NOT NULL,

  `seq_step_task_complete` varchar(200) NOT NULL,

  `seq_run_count` int(200) NOT NULL,

  `seq_last_run` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY (`seq_step_id`)

	);";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    dbDelta($sql);

    add_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);


    $installed_ver = get_option('custom_table_kb_plugin_db_version');

    if ($installed_ver != $custom_table_kb_plugin_db_version) {

        $sql = "CREATE TABLE " . $table_name . " (

  `seq_step_id` int(111) NOT NULL AUTO_INCREMENT,

  `seq_step_subject` varchar(200) NOT NULL,

  `seq_step_wait` varchar(200) NOT NULL,

  `seq_step_Delay_Times` varchar(123) NOT NULL,

  `seq_step_delaytime` varchar(200) NOT NULL,

  `seq_step_weekday` varchar(200) NOT NULL,

  `seq_step_type` varchar(200) NOT NULL,

  `seq_step_content` text NOT NULL,

  `seq_step_adddate` varchar(200) NOT NULL,

  `seq_step_email_email` varchar(200) NOT NULL,

  `seq_step_email_send_name` varchar(200) NOT NULL,

  `seq_step_email_rplay_email` varchar(200) NOT NULL,

  `seq_step_email_email_from` varchar(200) NOT NULL,

  `seq_step_tasks_due_date` varchar(200) NOT NULL,

  `seq_step_tasks_onwer` varchar(200) NOT NULL,

  `seq_step_tasks_onwer_type` varchar(111) NOT NULL,

  `seq_step_task_complete` varchar(200) NOT NULL,

  PRIMARY KEY (`seq_step_id`)

		);";


   require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
   dbDelta($sql);
   update_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);

    }

}



register_activation_hook(__FILE__, 'custom_table_kb_plugin_kb_sequence_step');

function custom_table_kb_plugin_setting()
{
    global $wpdb;
    global $custom_table_kb_plugin_db_version;

   $table_name = $wpdb->prefix .'kb_setting'; // do not forget about tables prefix
   $sql = "CREATE TABLE " . $table_name . " (
  			`id` int(200) NOT NULL AUTO_INCREMENT,
  			`kb_default_sponsor` varchar(200) NOT NULL,
  			`redirection_url` varchar(255) NOT NULL,
  			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2" ;

  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
  dbDelta($sql);
  add_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);

    $installed_ver = get_option('custom_table_kb_plugin_db_version');

    if ($installed_ver != $custom_table_kb_plugin_db_version) {

        $sql = "CREATE TABLE " . $table_name . " (
  			`id` int(200) NOT NULL AUTO_INCREMENT,
  			`kb_default_sponsor` varchar(200) NOT NULL,
  			`redirection_url` varchar(255) NOT NULL,
  			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2" ;
			
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

		dbDelta($sql);
		update_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);

    }

}
register_activation_hook(__FILE__, 'custom_table_kb_plugin_setting');

//////////////////////////////kb_info Start//////////////////////////////////////////////

function custom_table_kb_plugin_kb_sequence_cron_job()

{

    global $wpdb;

    global $custom_table_kb_plugin_db_version;



    $table_name = $wpdb->prefix .'kb_sequence_cron_job'; // do not forget about tables prefix

	$sql = "CREATE TABLE " . $table_name . " (

  `cron_id` int(111) NOT NULL AUTO_INCREMENT,

  `cron_aff_user_id` int(111) NOT NULL,

  `cron_seq_id` int(111) NOT NULL,

  `cron_seq_type_id` int(111) NOT NULL,

  `cron_seq_order` int(111) NOT NULL,

  `cron_seq_Delay_Times` varchar(1111) NOT NULL,

  `cron_date` varchar(111) NOT NULL,

  `cron_time` varchar(111) NOT NULL,

  `cron_execute` varchar(111) NOT NULL,

  `cron_status` varchar(111) NOT NULL DEFAULT '0',

  `cron_seq_type` varchar(111) NOT NULL,

  PRIMARY KEY (`cron_id`)

	);";
  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
  dbDelta($sql);
  add_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);

    $installed_ver = get_option('custom_table_kb_plugin_db_version');

    if ($installed_ver != $custom_table_kb_plugin_db_version) {

        $sql = "CREATE TABLE " . $table_name . " (

  `cron_id` int(111) NOT NULL AUTO_INCREMENT,

  `cron_aff_user_id` int(111) NOT NULL,

  `cron_seq_id` int(111) NOT NULL,

  `cron_seq_type_id` int(111) NOT NULL,

  `cron_seq_order` int(111) NOT NULL,

  `cron_seq_Delay_Times` varchar(1111) NOT NULL,

  `cron_date` varchar(111) NOT NULL,

  `cron_time` varchar(111) NOT NULL,

  `cron_execute` varchar(111) NOT NULL,

  `cron_status` varchar(111) NOT NULL DEFAULT '0',

  `cron_seq_type` varchar(111) NOT NULL,

  PRIMARY KEY (`cron_id`)

		);";
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

dbDelta($sql);
update_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);

    }

}



register_activation_hook(__FILE__, 'custom_table_kb_plugin_kb_sequence_cron_job');

//////////////////////////////kb_messages Start//////////////////////////////////////////////

function custom_table_kb_plugin_kb_messages()

{

    global $wpdb;

    global $custom_table_kb_plugin_db_version;



    $table_name = $wpdb->prefix .'kb_messages'; // do not forget about tables prefix

	$sql = "CREATE TABLE " . $table_name . " (

  `messages_id` int(11) NOT NULL AUTO_INCREMENT,

  `messages_name` varchar(111) NOT NULL,

  `messages_subject` varchar(111) NOT NULL,

  `messages_subject_email_task` varchar(200) NOT NULL,

  `messages_SendFromEmail` varchar(200) NOT NULL,

  `messages_SendFromName` varchar(200) NOT NULL,

  `messages_addNewUser` varchar(200) NOT NULL,

  `messages_ReplayToEmail` varchar(200) NOT NULL,

  `messages_addNewuser_email` varchar(200) NOT NULL,

  `messages_current_userID` varchar(200) NOT NULL,

  `messages_EmailFrom` varchar(200) NOT NULL,

  `messages_type` varchar(111) NOT NULL,

  `messages_content` text NOT NULL,

  `messages_sent` varchar(111) NOT NULL,

  `messages_dueDate` varchar(111) NOT NULL,

  `messages_date` date NOT NULL,

  `messages_date_add` date NOT NULL,

  PRIMARY KEY (`messages_id`)

	);";

    // we do not execute sql directly

    // we are calling dbDelta which cant migrate database

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    dbDelta($sql);



    // save current database version for later use (on upgrade)

    add_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);



    /**

     * [OPTIONAL] Example of updating to 1.1 version

     *

     * If you develop new version of plugin

     * just increment $custom_table_kb_plugin_db_version variable

     * and add following block of code

     *

     * must be repeated for each new version

     * in version 1.1 we change email field

     * to contain 200 chars rather 100 in version 1.0

     * and again we are not executing sql

     * we are using dbDelta to migrate table changes

     */

    $installed_ver = get_option('custom_table_kb_plugin_db_version');

    if ($installed_ver != $custom_table_kb_plugin_db_version) {

        $sql = "CREATE TABLE " . $table_name . "(

  `messages_id` int(11) NOT NULL AUTO_INCREMENT,

  `messages_name` varchar(111) NOT NULL,

  `messages_subject` varchar(111) NOT NULL,

  `messages_subject_email_task` varchar(200) NOT NULL,

  `messages_SendFromEmail` varchar(200) NOT NULL,

  `messages_SendFromName` varchar(200) NOT NULL,

  `messages_addNewUser` varchar(200) NOT NULL,

  `messages_ReplayToEmail` varchar(200) NOT NULL,

  `messages_addNewuser_email` varchar(200) NOT NULL,

  `messages_current_userID` varchar(200) NOT NULL,

  `messages_EmailFrom` varchar(200) NOT NULL,

  `messages_type` varchar(111) NOT NULL,

  `messages_content` text NOT NULL,

  `messages_sent` varchar(111) NOT NULL,

  `messages_dueDate` varchar(111) NOT NULL,

  `messages_date` date NOT NULL,

  `messages_date_add` date NOT NULL,

  PRIMARY KEY (`messages_id`)

		);";



        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        dbDelta($sql);



        // notice that we are updating option, rather than adding it

        update_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);

    }

}



register_activation_hook(__FILE__, 'custom_table_kb_plugin_kb_messages');

//////////////////////////////kb_sequence start//////////////////////////////////////////////

function custom_table_kb_plugin_kb_sequence()

{

    global $wpdb;

    global $custom_table_kb_plugin_db_version;



    $table_name = $wpdb->prefix .'kb_sequence'; // do not forget about tables prefix



    // sql to create your table

    // NOTICE that:

    // 1. each field MUST be in separate line

    // 2. There must be two spaces between PRIMARY KEY and its name

    //    Like this: PRIMARY KEY[space][space](id)

    // otherwise dbDelta will not work

	$sql = "CREATE TABLE " . $table_name . " (

  `seq_id` int(100) NOT NULL AUTO_INCREMENT,

  `seq_type` varchar(100) NOT NULL,

  `seq_type_id` int(11) NOT NULL,

  `seq_order` int(11) NOT NULL,

  `seq_adddate` varchar(111) NOT NULL,

  PRIMARY KEY (`seq_id`)

	);";

    // we do not execute sql directly

    // we are calling dbDelta which cant migrate database

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    dbDelta($sql);



    // save current database version for later use (on upgrade)

    add_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);



    /**

     * [OPTIONAL] Example of updating to 1.1 version

     *

     * If you develop new version of plugin

     * just increment $custom_table_kb_plugin_db_version variable

     * and add following block of code

     *

     * must be repeated for each new version

     * in version 1.1 we change email field

     * to contain 200 chars rather 100 in version 1.0

     * and again we are not executing sql

     * we are using dbDelta to migrate table changes

     */

    $installed_ver = get_option('custom_table_kb_plugin_db_version');

    if ($installed_ver != $custom_table_kb_plugin_db_version) {

        $sql = "CREATE TABLE " . $table_name . "(

  `seq_id` int(100) NOT NULL AUTO_INCREMENT,

  `seq_type` varchar(100) NOT NULL,

  `seq_type_id` int(11) NOT NULL,

  `seq_order` int(11) NOT NULL,

  `seq_adddate` varchar(111) NOT NULL,

  PRIMARY KEY (`seq_id`)

		);";



        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        dbDelta($sql);



        // notice that we are updating option, rather than adding it

        update_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);

    }

}



register_activation_hook(__FILE__, 'custom_table_kb_plugin_kb_sequence');

//////////////////////////////kb_smtp_settings Start//////////////////////////////////////////////

function custom_table_kb_plugin_kb_smtp_settings()

{

    global $wpdb;

    global $custom_table_kb_plugin_db_version;



    $table_name = $wpdb->prefix .'kb_smtp_settings'; // do not forget about tables prefix



	$sql = "CREATE TABLE " . $table_name . " (

  `smtp_id` int(100) NOT NULL AUTO_INCREMENT,

  `from_mail` varchar(100) NOT NULL,

  `smtp_host` varchar(100) NOT NULL,

  `smtp_encription` varchar(100) NOT NULL,

  `smtp_port` varchar(100) NOT NULL,

  `smtp_auth` varchar(100) NOT NULL,

  `from_name` varchar(100) NOT NULL,

  `smtp_user` varchar(100) NOT NULL,

  `smtp_pass` varchar(100) NOT NULL,

  PRIMARY KEY (`smtp_id`)

	);";

    // we do not execute sql directly

    // we are calling dbDelta which cant migrate database

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    dbDelta($sql);



    // save current database version for later use (on upgrade)

    add_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);



    /**

     * [OPTIONAL] Example of updating to 1.1 version

     *

     * If you develop new version of plugin

     * just increment $custom_table_kb_plugin_db_version variable

     * and add following block of code

     *

     * must be repeated for each new version

     * in version 1.1 we change email field

     * to contain 200 chars rather 100 in version 1.0

     * and again we are not executing sql

     * we are using dbDelta to migrate table changes

     */

    $installed_ver = get_option('custom_table_kb_plugin_db_version');

    if ($installed_ver != $custom_table_kb_plugin_db_version) {

        $sql = "CREATE TABLE " . $table_name . "(

		  `smtp_id` int(100) NOT NULL AUTO_INCREMENT,

		  `from_mail` varchar(100) NOT NULL,

		  `smtp_host` varchar(100) NOT NULL,

		  `smtp_encription` varchar(100) NOT NULL,

		  `smtp_port` varchar(100) NOT NULL,

		  `smtp_auth` varchar(100) NOT NULL,

		  `from_name` varchar(100) NOT NULL,

		  `smtp_user` varchar(100) NOT NULL,

		  `smtp_pass` varchar(100) NOT NULL,

		  PRIMARY KEY (`smtp_id`)

		);";



        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        dbDelta($sql);



        // notice that we are updating option, rather than adding it

        update_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);

    }

}



register_activation_hook(__FILE__, 'custom_table_kb_plugin_kb_smtp_settings');

//////////////////////////////kb_tasks start//////////////////////////////////////////////

function custom_table_kb_plugin_kb_tasks()

{

    global $wpdb;

    global $custom_table_kb_plugin_db_version;



    $table_name = $wpdb->prefix .'kb_tasks'; // do not forget about tables prefix



	$sql = "CREATE TABLE " . $table_name . " (

  `tasks_id` int(11) NOT NULL AUTO_INCREMENT,

  `tasks_subject` varchar(200) NOT NULL,

  `tasks_due_date` date NOT NULL,

  `tasks_add_date` date NOT NULL,

  `tasks_onwer` varchar(200) NOT NULL,

  `tasks_releted_to` varchar(200) NOT NULL,

  `tasks_status` varchar(200) NOT NULL,

  `tasks_Cron_id` int(11) NOT NULL,

  `tasks_content` text NOT NULL,

  PRIMARY KEY (`tasks_id`)

	);";

    // we do not execute sql directly

    // we are calling dbDelta which cant migrate database

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    dbDelta($sql);



    // save current database version for later use (on upgrade)

    add_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);



    /**

     * [OPTIONAL] Example of updating to 1.1 version

     *

     * If you develop new version of plugin

     * just increment $custom_table_kb_plugin_db_version variable

     * and add following block of code

     *

     * must be repeated for each new version

     * in version 1.1 we change email field

     * to contain 200 chars rather 100 in version 1.0

     * and again we are not executing sql

     * we are using dbDelta to migrate table changes

     */

    $installed_ver = get_option('custom_table_kb_plugin_db_version');

    if ($installed_ver != $custom_table_kb_plugin_db_version) {

        $sql = "CREATE TABLE " . $table_name . "(

  `tasks_id` int(11) NOT NULL AUTO_INCREMENT,

  `tasks_subject` varchar(200) NOT NULL,

  `tasks_due_date` date NOT NULL,

  `tasks_add_date` date NOT NULL,

  `tasks_onwer` varchar(200) NOT NULL,

  `tasks_releted_to` varchar(200) NOT NULL,

  `tasks_status` varchar(200) NOT NULL,

  `tasks_af_id` int(11) NOT NULL,

  `tasks_content` text NOT NULL,

   PRIMARY KEY (`tasks_id`)

		);";



        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        dbDelta($sql);



        // notice that we are updating option, rather than adding it

        update_option('custom_table_kb_plugin_db_version', $custom_table_kb_plugin_db_version);

    }

}



register_activation_hook(__FILE__, 'custom_table_kb_plugin_kb_tasks');





/**

 * Trick to update plugin database, see docs

 */

function custom_table_kb_plugin_update_db_check()

{

    global $custom_table_kb_plugin_db_version;

    if (get_site_option('custom_table_kb_plugin_db_version') != $custom_table_kb_plugin_db_version) {

        custom_table_kb_plugin_install();

    }

}



add_action('plugins_loaded', 'custom_table_kb_plugin_update_db_check');



// End The Table insert

//include_once('modules/table/kb-install_table.php');

defined( 'ABSPATH' ) or die( 'Plugin file cannot be accessed directly.' );



	/*

	* this for add menu 

	*/

	

	/*add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position ); */

	function register_menuOnDeshboard() {

		add_menu_page('KB Plugin', 'KB Plugin', 'edit_posts', 'kb-plugin', '_custom_menu_page', null, 6); 

		

		add_submenu_page('kb-plugin','Affiliates', 'Affiliates', 'edit_posts', 'kb-plugin', '_custom_menu_page');

			

		add_submenu_page('kb-plugin','Tasks', 'Tasks', 'edit_posts', 'tasks', '_all_tasks'); 			// Task Manu

			

		add_submenu_page('kb-plugin','Messages', 'Messages', 'add_users', 'messages', '_all_messages');  // Messages Manu

	

		add_submenu_page('kb-plugin','Sequence', 'Sequence', 'add_users', 'sequence', '_sequence');  // Sequence

		

		add_submenu_page('kb-plugin','Settings', 'Settings', 'add_users', 'smtp-setting', '_smtpSetting');  // SMTP

		

		//add_submenu_page('kb-plugin','Cron Setting', 'Cron Job', 'add_users', 'cron_job', '_sequenceCron');  // _sequenceCron

	   
	    add_submenu_page('kb-plugin','About', 'About', 'add_users', 'About', '_aboutShortcode');  // _sequenceCron

	}

	

	add_action('admin_menu', 'register_menuOnDeshboard');

	

	

	function _custom_menu_page(){

		include_once('modules/affiliated/kb-affiliated.php');

	}

	

	// Start By Amin Khan 01 June 2015 MANU //

	function _all_tasks(){

		include_once('modules/tasks/kb-tasks.php');

	}

	

	function _all_messages(){

		include_once('modules/messages/kb-messages.php');

	}

	

	function _smtpSetting(){

		include_once('modules/mail/smtp-setting.php');

	}

	

	function _sequence() {
		
		include_once('modules/sequence/kb-sequence.php');

	}

	function _sequenceCron() {

		include_once('modules/cron_job/cron_job.php');

	}	

	function wpb_mce_buttons_2($buttons) {

 

		array_unshift($buttons, 'styleselect');

	 

		return $buttons;

	}

	 

	add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

	// Add Shortcode

	function Affiliates_shortcode() { 
                  ob_start();
		  include_once('modules/affiliated/affi-add.php');
                  return ob_get_clean();
	 }

	add_shortcode( 'Affiliates', 'Affiliates_shortcode' );

	

	

	

	//Cron job Setting Function 

	function cron_add_minute( $schedules ) {

		// Adds once every minute to the existing schedules.

		$schedules['everyminute'] = array(

			'interval' => 60,

			'display' => __( 'Once Every Minute' )

		);

		return $schedules;

	}

	add_filter( 'cron_schedules', 'cron_add_minute' );



	register_activation_hook( __FILE__, 'Cronsetup' );



	function Cronsetup(){ 

		if ( ! wp_next_scheduled( 'my_task_hook2' ) ) {

			  wp_schedule_event( time(), 'everyminute', 'my_task_hook2' );

		}

		add_action( 'my_task_hook2', 'my_task_function' );

	}

	add_shortcode( 'Kb_plugin_cron', 'croncallFunction' );

	

	function croncallFunction(){

		

		include_once('modules/cron_job/cron_job.php');

	}
	
	function _aboutShortcode(){
		
		  include_once('modules/about/index.php');
		}

	//my_task_function();