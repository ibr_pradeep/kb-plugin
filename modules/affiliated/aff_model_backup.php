<?php

	

class Affimodel implements model
{
	
	public $db;
	public function foo($x)
	{
	  return true;	
	}
	
	/*
	* Get Affiliated Data for Data table
	*/
	public function GetData()
	{
	   global $post,$wpdb;
	   $user_ID = get_current_user_id();
	   
	   	  $users		 = $wpdb->prefix."users";
		  $kb_affiliated = $wpdb->prefix."kb_affiliated";
		  $permission = $this->GetcurrentUserRol($user_ID);
		  
		 if($permission['administrator']) {
		 	 $Sql="select * from $kb_affiliated left JOIN $users ON $kb_affiliated.kb_signup_by = $users.ID"; 
	  	 	 $result = $wpdb->get_results($Sql);
		 }else {
			 $Sql="select * from $kb_affiliated left JOIN $users ON $kb_affiliated.kb_signup_by = $users.ID"; 
			 //$Sql="select * from $kb_affiliated left JOIN $users ON $kb_affiliated.kb_signup_by = $users.ID where kb_signup_by = $user_ID"; 
	  	 	 $result = $wpdb->get_results($Sql);
		 }
	  return $result;
	}
	
	public function GetcurrentUserRol($user_ID){
		 global $post,$wpdb;
		 $Sql="select meta_value from ".$wpdb->prefix."usermeta where meta_key = 'wp_capabilities' AND user_id = ".$user_ID; 
		  $Capablities = $wpdb->get_results($Sql);
		  $serialize = $Capablities[0]->meta_value;
		  $unserialize = array();
		  $unserialize = unserialize($serialize);
		  return $unserialize;
	}
	
	/*
	* GetAffiliData by id
	*/
	public function GetAffiliData($id)
	{
		global $post,$wpdb;
		$kb_affiliated= $wpdb->prefix."kb_affiliated";
		$Sql = "select * from $kb_affiliated
				where af_id=".$id;
		$result = $wpdb->get_results($Sql);
		return $result;
	}
	/**
	* Update affiliated  
	*/
	public function UpdateAfiliated($postdata)
	{
		global $post, $wpdb;
		$kb_affiliated= $wpdb->prefix."kb_affiliated";
		$DOB = date('Y-m-d', strtotime($postdata['birth_day']));
		$wpdb->update("$kb_affiliated", 
			  array( "af_fname" 		=>  $postdata['f_name'],
					  "af_lname" 		=>	$postdata['l_name'],
					  "af_state" 		=>  $postdata['state'],
					  "af_email" 		=>  $postdata['e_mail'],
									   
					  "af_dob" 			=>  $DOB,
					  "af_address"		=>  $postdata['add'],
					  "af_city" 		=>  $postdata['city'],
					  "af_state" 		=>  $postdata['state'],
					  "af_zipcode"		=>  $postdata['zip_code'],
					  "af_country"		=>  $postdata['country'],
					  "af_mo_no" 		=>  $postdata['mobile_no']
					  ),
					  array( 'af_id' => $postdata['af_id'] )
		  
		); 
	}	
	
	/*
	* GetKaratbarsData by id
	*/
	public function GetKaratbarsData($id)
	{
		global $post,$wpdb;
		$kb_affiliated= $wpdb->prefix."kb_affiliated";
		$Sql = "select * from $kb_affiliated
				where af_id=".$id;
		$result = $wpdb->get_results($Sql);
		return $result;
	}
		
	/**
	* Add new Karatbarsed
	* @ true 
	*/	 
	public function InsertKaratbarsed($postdata)
	{
	global $post, $wpdb;
	$kb_affiliated= $wpdb->prefix."kb_affiliated";
	$wpdb->insert("$kb_affiliated", 
			array("kb_username" 		=>  $postdata['kb_username'],
				  "kb_password" 		=>	$postdata['kb_password'],
				  "kb_team_side" 		=>  $postdata['kb_team_side'],
				  "kb_id_proof" 		=>  $postdata['kb_id_proof'],									   
				  "kb_add_proof" 		=>  $postdata['kb_add_proof'],
				  "kb_package"			=>  $postdata['kb_package'],
				  "kb_sponsor" 			=>  $postdata['kb_sponsor'],
				  "kb_signup_by" 		=>  $postdata['kb_signup_by'],
				  "kb_acc_created"		=>  $postdata['kb_acc_created'],
				  "kb_auto_payment"		=>  $postdata['kb_auto_payment']
				  )
		  );
	  return true;
	}
	/**
	* Update Karatbars  
	*/
	public function UpdateKaratbarsed($postdata)
	{
		global $post, $wpdb;
		$kb_affiliated= $wpdb->prefix."kb_affiliated";
		$wpdb->update("$kb_affiliated", 
			  array(  "kb_username" 		=>  $postdata['kb_username'],
					  "kb_password" 		=>	$postdata['kb_password'],
					  "kb_team_side" 		=>  $postdata['kb_team_side'],
					  "kb_id_proof" 		=>  $postdata['kb_id_proof'],
					  "kb_add_proof" 		=>  $postdata['kb_add_proof'],
					  "kb_package"			=>  $postdata['kb_package'],
					  "kb_sponsor" 			=>  $postdata['kb_sponsor'],
					  "kb_signup_by" 		=>  $postdata['kb_signup_by'],
					  "kb_acc_created"		=>  $postdata['kb_acc_created'],
					  "kb_auto_payment"		=>  $postdata['kb_auto_payment']
				  
					  ),
					  array( 'af_id' => $postdata['af_id'] )
		  
		); 
	}
	
	/*
	* Delete Affiliate
	*/
	public function DeleteAffiliates($id)
	{
		global $post, $wpdb;
		$kb_affiliated= $wpdb->prefix."kb_affiliated";
		$count = $wpdb->delete( "$kb_affiliated", array('af_id'=>$id), $where_format = null );
		//$count = $wpdb->delete( 'kb_info', array('kb_af_id'=>$id), $where_format = null );
		return $count;
	}
	public function delete_af_id($postdata)
	{
		global $post, $wpdb;
		$kb_affiliated= $wpdb->prefix."kb_affiliated";
		$wpdb->delete("$kb_affiliated", array("af_id" =>  $postdata['af_id']) );
		//$wpdb->delete("kb_info", array("kb_af_id" =>  $postdata['af_id']) );
		return true;
	}	
	/**
	* Add new Affiliated
	* @ true 
	*/	 
	public function InsertAfiliated($postdata)
	{
		
	global $post, $wpdb;
	$kb_affiliated= $wpdb->prefix."kb_affiliated";
	$kb_sequence_cron_job 	= $wpdb->prefix."kb_sequence_cron_job";
	$DOB = date('Y-m-d', strtotime($postdata['birth_day']));
	$wpdb->insert("$kb_affiliated", 
			array("af_fname" 		=>  $postdata['f_name'],
				  "af_lname" 		=>	$postdata['l_name'],
				  "af_state" 		=>  $postdata['state'],
				  "af_email" 		=>  $postdata['e_mail'],	
				  "kb_username" 	=>  $postdata['kb_username'],
				  "kb_password" 	=>	$postdata['kb_password'],								   
				  "af_dob" 			=>  $DOB,
				  "af_address"		=>  $postdata['add'],
				  "af_city" 		=>  $postdata['city'],
				  "af_state" 		=>  $postdata['state'],
				  "af_zipcode"		=>  $postdata['zip_code'],
				  "af_country"		=>  $postdata['country'],
				  "af_mo_no" 		=>  $postdata['mobile_no'],
				  "kb_sponsor"		=>  $postdata['kb_sponcer'],
				  "af_add_date"		=>  date("Y-m-d h:i:s") // current_time( 'mysql' ) // ... and so on
				  )
		  );
			$lastInsertAffiliateId 					= $wpdb->insert_id;
			//include('../cron_job/functions.php');
			
			
			
			$res						= $this->CronFunction($lastInsertAffiliateId);
			//$GetSequenceOrder 		= $this->EnterSequenceOrder($lastid);
	  return true;
	}
	
	public function CronFunction($id='')
	{	
		global $post,$wpdb;
		$CheckSequenceCronjob 		= $this->CheckSequenceCronjob($id);
		
		$kb_sequence_cron_job 	= $wpdb->prefix."kb_sequence_cron_job";
		$Sql					=	"select * from `$kb_sequence_cron_job`
	   				 		 		WHERE `cron_status`= '0' AND `is_paused` = '0'";
		$result 				= $wpdb->get_results($Sql); 
		
		
		
		if(!empty($result)) { $i= 1;
			foreach($result as $data) 
			{		
					$cron_id 	= $data->cron_id;
					 $this->ExecuteSequnce($cron_id);
					
			}
		} 
		else { 
				
		}
	}
	
	/**
	* Execution Sequnce All check
	* @ true 
	*/	 
	
	public function ExecuteSequnce($Cron_id)
	{ 	 
		global $post, $wpdb;
		$kb_sequence_cron_job 	= $wpdb->prefix."kb_sequence_cron_job";
		$Sql					= "select * from $kb_sequence_cron_job	where cron_id=".$Cron_id;
		$result 				= $wpdb->get_results($Sql);
		
		
		
		$date  					= strtotime(date("Y-m-d H:i:s", time())); //date("Y-m-d H:i:s", time()); 
		
		foreach( $result as $result1)
		{  
		    $cron_date = strtotime($result1->cron_date); 
		
		   if($cron_date<=$date)
		   { 	 
		   		$cron_id			= $result1->cron_id;
				$cron_aff_user_id 	= $result1->cron_aff_user_id;
				$cron_seq_type_id 	= $result1->cron_seq_type_id;
				$seq_order 			= $result1->cron_seq_order;
				$cron_date 			= $result1->cron_date;
				$cron_date 			= $result1->cron_date;
				
				$data = array(	"cron_aff_user_id" 	=>$cron_aff_user_id,
								"cron_seq_type_id"	=>$cron_seq_type_id,
								"cron_id" 			=>$cron_id,
								"seq_order" 		=>$seq_order,
								"cron_date"			=>$cron_date);
							
		   		if($result1->cron_seq_type=="Task")
				{  
					$result_data = $this->insert_Sequence_Taskcreat($data);
					 if($result_data==true)
					   {
						 $result_data = $this->update_Sequence_cronjob($result1);  
						 $this->UpdateStepRunCount_AndLastRunTime($result1->cron_seq_id); 
						 return true;
					   }
					   else {  return false;  }
					 					   
				}
				else
				{	
					$result_data = $this->Email_Sequence_send($data);
					$this->UpdateStepRunCount_AndLastRunTime($result1->cron_seq_id);
					if($result_data==true)
					   { 
						 $result_data = $this->update_Sequence_cronjob($data);  
						 return true;
					   }
					   else {  return false; }
				}
		   }
		 
		}
		 
	}
   	
	/*****************
	# New User Mail Send Sequence  Cron
	******************/
	public function CheckSequenceCronjob($id=''){
		global $post,$wpdb; 
		$kb_sequence 			= $wpdb->prefix."kb_sequence";
		$kb_sequence_cron_job 	= $wpdb->prefix."kb_sequence_cron_job";
			
		if($id == ''){
				$Sql				= "select cron_aff_user_id from `$kb_sequence_cron_job`
									   WHERE `cron_status`= '1' 
									   AND is_paused = 0
									   group BY `cron_aff_user_id`";
									   
			   $AFFUSerIDs = $wpdb->get_results($Sql); 
			   
			   $result = array();
			foreach($AFFUSerIDs as $affuser){
				$Sql				= "select * from `$kb_sequence_cron_job`
									   WHERE `cron_status`= '1' 
									   AND is_paused = 0";
				 $Sql .= " AND cron_aff_user_id = ".$affuser->cron_aff_user_id;
							   
				$Sql .=   " ORDER BY `cron_seq_order` DESC LIMIT 1";
				$MaxSeqUser = $wpdb->get_results($Sql); 
				$result[] = $MaxSeqUser[0];
				//echo $wpdb->last_query;
				
			}
			
		}else{
		/*$Sql					= "select * from `$kb_sequence_cron_job`
	   				 		 	   WHERE `cron_aff_user_id`= '".$id."' 
								   ORDER BY `cron_seq_order` DESC LIMIT 1";*/
				$Sql  = "select * from `$kb_sequence_cron_job`
						 WHERE `cron_status`= '1' 
						 AND is_paused = 0";
				$Sql .=" AND cron_aff_user_id = ".$id;
				$Sql .=   " ORDER BY `cron_seq_order` DESC LIMIT 1";
				$result = $wpdb->get_results($Sql); 
		}
		
		if(!empty($result)) {	
			foreach($result as $res) { 
				$id				=	$res->cron_aff_user_id;
				$cron_seq_order	= $res->cron_seq_order + 1;
				$cron_date		= $res->cron_date;
				$cron_status	= $res->cron_status;
				if(($cron_status==1) && ($res->is_paused == 0)){
					$NextSeqNotAvailable = $this->CheckNextSeqArePresentOrNot($id, $cron_seq_order);
					
					if($NextSeqNotAvailable == true){
							$insertSequenceOrderJob	= $this->insertSequenceOrderJob($id,$cron_seq_order,$cron_date);
							if($insertSequenceOrderJob == true){
						
							}
					}
					//return true;
				}
			}
		}else { 
			$cron_seq_order				=  1;
			$insertSequenceOrderJob 	= $this->insertSequenceOrderJob($id,$cron_seq_order);
						
		}
	}	

	public function CheckNextSeqArePresentOrNot($AffUserId, $NextSeqOrderID){
		global $post,$wpdb; 
		$kb_sequence_cron_job 	= $wpdb->prefix."kb_sequence_cron_job";
		$Sql = "select * from `$kb_sequence_cron_job`
	   		    WHERE `cron_aff_user_id`= '".$AffUserId."' 
				AND `is_paused`= '1'
			    AND `cron_seq_order` = '".$NextSeqOrderID."'";
		$result = $wpdb->get_results($Sql);
		if(!empty($result)){
			return false;	
		}else{
			return true;	
		}
	}
	/*****************
	# Update Task Cron job Sequence 
	******************/
	public function update_Sequence_cronjob($data='')
	{		
			global $post, $wpdb;
			if(is_array($data)){
				$data = (object)$data;
			}
			
			//$data 						=$userid;
			$kb_sequence_cron_job 		= $wpdb->prefix."kb_sequence_cron_job";
			$id 						= $data->cron_aff_user_id;
			$seq_id 					= $data->cron_seq_id;
			$seq_type_id 				= $data->cron_seq_type_id;
			$seq_order					= $data->cron_seq_order;
			$seq_type 					= $data->cron_seq_type;
			$datetime				 	= $data->cron_date;
			
	   		$res = $wpdb->update("$kb_sequence_cron_job", 
		  			array( 	"cron_execute" 	  		=> "DONE",
							"cron_status" 	  		=> 1
					),
					  array( 'cron_id' 				=> $data->cron_id)
					);
			if($res==true) { 
				
			 	//$add =  $this->CheckSequenceCronjob($id);
				$res = $this->CronFunction($id);
			} 
	}
	
	
	/*****************
	# New User Mail Send Sequence  Cron
	******************/
	public function GetSequenceOrder($id='') {
		global $post,$wpdb;
		$kb_sequence 		= $wpdb->prefix."kb_sequence";
		$kb_sequence_step 	= $wpdb->prefix."kb_sequence_step";
		$Sql				="select * from $kb_sequence left JOIN $kb_sequence_step 
							ON $kb_sequence.seq_type_id = $kb_sequence_step.seq_step_id";
		if (!empty($id)) { 
			$Sql			.=" WHERE $kb_sequence.seq_order= '$id'";
		} else {
		 	$Sql			.=" ORDER BY  `$kb_sequence`.`seq_order` ASC LIMIT 1 ";	
		}	
		
		$result = $wpdb->get_results($Sql);  
		if(!empty($result))
		{
			return $result;
		}
		else {	return false;	}
		
	}
	
	/*****************
	# New User Mail Send Sequence  Data 
	******************/
	public function GetSequenceDate_step($seq_step_id='')
	{
		global $post,$wpdb;
		$kb_sequence_step 	= $wpdb->prefix."kb_sequence_step";
		$Sql				="select * from $kb_sequence_step 
							 WHERE seq_step_id = '$seq_step_id'";
		$result 			= $wpdb->get_results($Sql); 
		return $result;
	}
	
	/*****************
	# New User Cron Job Insert Sequence 
	******************/
	public function insertSequenceOrderJob($id='',$seq_orderId='',$excuteDate='')
	{		
			global $post, $wpdb;
			$kb_sequence_cron_job 		= $wpdb->prefix."kb_sequence_cron_job";
			
			$GetSequenceOrder 			= $this->GetSequenceOrder($seq_orderId); 
			
			if ($GetSequenceOrder != false) {
					$seq_id 					= $GetSequenceOrder[0]->seq_id;
					$seq_type_id 				= $GetSequenceOrder[0]->seq_type_id;
					$seq_order					= $GetSequenceOrder[0]->seq_order;
					$seq_type 					= $GetSequenceOrder[0]->seq_type;
					$seq_step_tasks_due_date 	= $GetSequenceOrder[0]->seq_step_tasks_due_date;
					$seq_step_tasks_due_date 	= $GetSequenceOrder[0]->seq_step_tasks_due_date;
					$seq_step_Delay_Times		= $GetSequenceOrder[0]->seq_step_Delay_Times;
					$seq_step_weekday 			= $GetSequenceOrder[0]->seq_step_weekday;
				
					if(!empty($seq_step_weekday)){
						$weeked 				= explode(',',$seq_step_weekday);
						$date 					= $this->enter_Sequence_dateTime($GetSequenceOrder,$id,$excuteDate);
						$datetime				= $this->daycal($date,$weeked);
					}
					else {
						$datetime 				= $this->enter_Sequence_dateTime($GetSequenceOrder,$id,$excuteDate);
					}
					
					$wpdb->insert("$kb_sequence_cron_job",
								array(	"cron_aff_user_id" 	 	=> $id,
										"cron_seq_id" 	  		=> $seq_id,
										"cron_seq_type_id" 		=> $seq_type_id,
										"cron_seq_order" 		=> $seq_order,
										"cron_date" 	  		=> $datetime,
										"cron_time" 	  		=> '',
										"cron_execute" 	  		=> "NOT",
										"is_paused"				=> 0,
										"cron_status" 	  		=> 0,
										"cron_seq_type" 		=> $seq_type
										)
									);
					 return true;
			}else { 
				return false;
			}
	}
	
		
	public function daycal($date='',$days='') {
		
			$Todayday = date("l");  
			
			if(in_array("$Todayday", $days)){
			 	$date_waitday  	= $date;
				return 	$date_waitday;
			}else{
				for($i=3; $i<=9; $i++)	{
				
					$weekday = date('D', strtotime("+$i day", $date));
					$FulldayName = $this->GetFulldayName($weekday);
					if(in_array("$FulldayName", $days)){
							
						$date_waitday = date('Y-m-d', strtotime("next $FulldayName", strtotime($date)));
						return 	$date_waitday;
					}
					
				}
			}
			/*{
				if(in_array("Monday", 	$day)){ $date_waitday  		= date("Y-m-d h:i:s"  ,strtotime("next Monday $date") ); }
				if(in_array("Tuesday", 	$day)){ $date_waitday  		= date("Y-m-d h:i:s"  ,strtotime("next Tuesday $date") ); }
				if(in_array("Wednesday",$day)){ $date_waitday  	= date("Y-m-d h:i:s"  ,strtotime("next Wednesday $date") ); }
				if(in_array("Thursday", $day)){ $date_waitday  		= date("Y-m-d h:i:s"  ,strtotime("next Thursday $date") ); }
				if(in_array("Friday", 	$day)){ $date_waitday  		= date("Y-m-d h:i:s"  ,strtotime("next Friday $date") ); }
				if(in_array("Saturday", $day)){ $date_waitday  		= date("Y-m-d h:i:s"  ,strtotime("next Saturday $date") ); }
				if(in_array("Sunday", 	$day)){ $date_waitday  		= date("Y-m-d h:i:s"  ,strtotime("next Sunday $date") ); }
			}*/
		 
		
			//return 	$date_waitday;
	}
	
	public function GetFulldayName($dayname){
		if($dayname == "Sun"){
			return "Sunday";	
		}elseif($dayname == "Mon"){
			return "Monday";	
		}elseif($dayname == "Tue"){
			return "Tuesday";	
		}elseif($dayname == "Wed"){
			return "Wednesday";	
		}elseif($dayname == "Thu"){
			return "Thursday";	
		}elseif($dayname == "Fri"){
			return "Friday";	
		}elseif($dayname == "Sat"){
			return "Saturday";	
		}
	}
	
	/*****************
	# User Data information Cron job Sequence 
	******************/
	public function Get_data_affilit_cronjob($userid='')
	{		
			global $post, $wpdb;
			$id 			= $userid;
			$kb_affiliated 	= $wpdb->prefix."kb_affiliated";
			$Sql			= "select * from $kb_affiliated	where af_id=".$id;
			$result 		= $wpdb->get_results($Sql);
			return $result;
	}
	  /*****************
	  # Task Creat Cron job Sequence 
	  ******************/
	public function enter_Sequence_dateTime($GetSequenceOrder='',$Newuser_mailSequence='',$excuteDate='')
	{   
		
		$datetime='';
		
		$Newuser_mailSequence 		= $this->Get_data_affilit_cronjob($Newuser_mailSequence);
		
		if(!empty($excuteDate)) {
			$addDate 				= $excuteDate; 
		} else {
			$addDate 				= $Newuser_mailSequence[0]->af_add_date; 
		}
		$seq_id 					= $GetSequenceOrder[0]->seq_id;
		$seq_type_id 				= $GetSequenceOrder[0]->seq_type_id;
		$seq_order					= $GetSequenceOrder[0]->seq_order;
		$seq_type 					= $GetSequenceOrder[0]->seq_type;
		$seq_step_wait 				= $GetSequenceOrder[0]->seq_step_wait;
		$seq_step_delaytime 		= $GetSequenceOrder[0]->seq_step_delaytime; 
		$seq_step_Delay_Times		= $GetSequenceOrder[0]->seq_step_Delay_Times; 
		$seq_step_weekday 			= $GetSequenceOrder[0]->seq_step_weekday;
		$seq_step_tasks_due_date 	= $GetSequenceOrder[0]->seq_step_tasks_due_date;
		$seq_step_Delay_Times		= $GetSequenceOrder[0]->seq_step_Delay_Times;
		$weeked 					= explode(',',$seq_step_weekday);
		if($seq_step_Delay_Times=="Times")
		{  
		  $date11 					= date('Y-m-d',strtotime($addDate));
		  $date_waitday1  			= date("Y-m-d H:i:s", strtotime("$date11 $seq_step_delaytime") );
		  $datetime   				= $date_waitday1; 
		}
		else
		{ 
		  if($seq_step_delaytime == "Immediately")
		  {   
			  $date_waitday1  		= date("Y-m-d h:i:s", strtotime( "$addDate +$seq_step_wait day" ) );
			   $datetime   			= $date_waitday1; 
		  }
		  else
		  {  
			  $date_waitday  		= date( "Y-m-d H:i:s", strtotime( "$addDate +$seq_step_wait day" ) );
			  $date_waitdaynew_time = date("Y-m-d H:i:s", strtotime("$date_waitday +$seq_step_delaytime"));
			  $datetime   			= $date_waitdaynew_time; 
		  }
		}
		return $datetime;
	}	
	
	public function GetOwnerGropby($SeqStepID){
		global $post, $wpdb;
		$kb_sequence_step	= $wpdb->prefix."kb_sequence_step";
		$Sql				= "select * from $kb_sequence_step	where seq_step_id=".$SeqStepID;
		$StepData 			= $wpdb->get_results($Sql);
		if($StepData['seq_step_tasks_onwer_type'] == 'Group'){
			$args = array(
				'meta_value'   => '',
			);
			get_users( $args );
			$result = GetAllGroupUsers($StepData);
		}
		
	}
	
	/*****************
	# Task Create according to Cron job Sequence 
	******************/
	public function insert_Sequence_Taskcreat($data='')
	{  //echo "<pre>"; print_r($data); die;
		global $post, $wpdb;
		$kb_tasks						= $wpdb->prefix."kb_tasks";
		$seq_order 						= $data['seq_order'];
		$seq_step_id 					= $data['cron_seq_type_id'];
		//$this->GetOwnerGropby($data['cron_seq_type_id']);

		$GetSequenceOrder	= $this->GetSequenceDate_step($seq_step_id);
		//echo"<pre>"; print_r($GetSequenceOrder); die;
		
		$userid							= $data['cron_aff_user_id'];
		$seq_type_id					= $data['cron_seq_type_id'];
		
		$userinfo						= $this->Get_data_affilit_cronjob($userid);
		$messeges						= $this->GetSequenceDate_step($seq_type_id);
		$mailbody						= $this->str_rwplace($messeges,$userinfo);
		
		$seq_step_subject 				= $GetSequenceOrder[0]->seq_step_subject;
		$seq_step_tasks_onwer 			= $GetSequenceOrder[0]->seq_step_tasks_onwer;
		$seq_adddate					= $GetSequenceOrder[0]->seq_step_adddate;
		$seq_step_adddate 				= $GetSequenceOrder[0]->seq_step_adddate;
		$seq_step_task_complete 		= $GetSequenceOrder[0]->seq_step_task_complete;
		
		$dueDayCount = $GetSequenceOrder[0]->seq_step_tasks_due_date;
		$dueDate =	date('Y-m-d', strtotime("+$dueDayCount day", time()));
		$CurrentDate =	date('Y-m-d');
		
		if($GetSequenceOrder[0]->seq_step_tasks_onwer_type == 'Group'){
				$GroupName = $GetSequenceOrder[0]->seq_step_tasks_onwer;
				$ownerID = $this->GetRounRobinOwnerID($GroupName);
				$seq_step_tasks_onwer = $ownerID;
		
		}
			
		if($seq_step_task_complete==1) {
	    	$seq_step_task_complete1		= 'In Progress';
		}
		
		$seq_step_tasks_due_date 		= $GetSequenceOrder[0]->seq_step_adddate;
		
		$seq_step_content     			= $mailbody;
		
		$wpdb->insert("$kb_tasks", array( "tasks_subject" 	=>  $seq_step_subject,
										  "tasks_due_date" 	=>	$dueDate, //$seq_adddate,
										  "tasks_onwer" 	=>  $seq_step_tasks_onwer,
										  "tasks_releted_to"=>  $userid,									   
										  "tasks_status" 	=>  $seq_step_task_complete1,
										  "tasks_content" 	=>  $seq_step_content,
										  "tasks_Cron_id" 	=>  $data['cron_id'],
										  "tasks_add_date"	=>  $CurrentDate // ... and so on
									  )
									  
				  );
		$lastInsertTaskId = $wpdb->insert_id;
		
		
		
		// This UPdation for add update signup by field for Affilied user
		$wpdb->update(	$wpdb->prefix."kb_affiliated",
							array('kb_signup_by' => $seq_step_tasks_onwer),
							array('af_id' 		 => $userid)
						 );	
						 
		
		
		if($GetSequenceOrder[0]->seq_step_task_complete == 1){
			$wpdb->update(	$wpdb->prefix."kb_sequence_cron_job",
							array('is_paused' => 1),
							array('cron_id' => $data['cron_id'])
						 );	
		}
		//$this->Email_Sequence_send($seq_step_id);
		//$this->Email_Sequence_sendOn($seq_step_id);
	
		$tr= $this->Email_Sequence_sendOn($GetSequenceOrder,$data,$seq_step_tasks_onwer);
		return true;
	}
	
	public function GetRounRobinOwnerID($Grouptype){
		global $post, $wpdb;
		
		$Grouptype = strtolower($Grouptype);
		
				$userIDs = get_users();
				$groupMember = array();
				
				foreach($userIDs as $user){
					$roles = $user->roles;
					if(in_array($Grouptype,$roles)){
						$groupMember[] = $user->data->ID;
					}
				}
		 
		$kb_tasks			= $wpdb->prefix."kb_tasks";
		$Sql				= "SELECT tasks_onwer from $kb_tasks ORDER BY tasks_id DESC LIMIT 1";
		$highestID 			= $wpdb->get_results($Sql);
		$highestID 			= $highestID[0]->tasks_onwer;
		
		if(empty($highestID)){
			return $groupMember[0];
		}
		
		for($i=0; $i<count($groupMember); $i++){
			
			if($highestID == $groupMember[$i]){
				if(($i+1)<count($groupMember)){
					return $groupMember[$i+1];
				}elseif($i+1==count($groupMember)){
					return $groupMember[0];
				}
			}
		}
		
	}

	/*****************
	# Key word Replacement Templete Function 
	******************/
	function str_rwplace($messagebody='',$userinfo='')

	{	
		$af_fname 				= $userinfo[0]->af_fname;
		$af_lname 				= $userinfo[0]->af_lname;
		$af_email 				= $userinfo[0]->af_email;
		$af_dob 				= date('d/M/Y',strtotime($userinfo[0]->af_dob));
		$af_address 			= $userinfo[0]->af_address;
		$af_city 				= $userinfo[0]->af_city;
		$af_state 				= $userinfo[0]->af_state;
		$af_zipcode				= $userinfo[0]->af_zipcode;
		$af_country 			= $userinfo[0]->af_country;
		$af_mo_no 				= $userinfo[0]->af_mo_no;
		$kb_username			= $userinfo[0]->kb_username;
		$kb_password			= $userinfo[0]->kb_password;
		$kb_sponsor				= $userinfo[0]->kb_sponsor;
		$content 				= $messagebody[0]->seq_step_content;
		
		$search 	= array('[First Name]', 
							'[Last Name]', 
							'[Email]', 
							'[Birthday]', 
							'[Address]', 
							'[City]', 
							'[State]', 
							'[Zip Code]', 
							'[Country]', 
							'[Mobile Phone]', 
							'[Username]', 
							'[Password]', 
							'[Sponsor]');
		
		$replace 	= array("$af_fname", 
							"$af_lname", 
							"$af_email", 
							"$af_dob", 
							"$af_address", 
							"$af_city", 
							"$af_state", 
							"$af_zipcode", 
							"$af_country", 
							"$af_mo_no", 
							"$kb_username", 
							"$kb_password",
							"$kb_sponsor");
		
		$content_r	= str_replace($search, $replace, $content);
		
		return $content_r;
	}
	
	////////////////////////////////// 
	// Function for form mail ID and From Name by msg Id
	// @return array 
	/////////////////////////////////
	public function GetFromMailIdForMessage($MessageId){
			global $post, $wpdb,$server ;
			$kb_messages 	= $wpdb->prefix."kb_messages";
			$Sql			= "select * from $kb_messages	
								where messages_id=".$MessageId;
			$messageData 		= $wpdb->get_results($Sql);
			
			$userDetails = $this->Get_data_Owner_cronjob($messageData[0]->messages_SendFromEmail);
			
			$return = array(
						"FromMail" => $userDetails[0]->user_email,
						"FromName" => $userDetails[0]->display_name
						);
			return $return;
	}
	
	public function Email_Sequence_sendOn($GetSequenceOrder='',$userinfo='', $Taskownerid='')
	{	
		
		date_default_timezone_set('asia/calcutta');
		global $post, $wpdb,$server ;
		require_once('PHPMailer/class.phpmailer.php');
		require_once('PHPMailer/class.smtp.php');
		
		if(!empty($userinfo['cron_aff_user_id'])){ $userid				  = $userinfo['cron_aff_user_id'];}
		if(!empty($userinfo['cron_seq_type_id'])){ $seq_type_id			  = $userinfo['cron_seq_type_id'];}
		
		if($GetSequenceOrder[0]->seq_step_tasks_onwer_type != 'Group'){
			if(!empty($GetSequenceOrder[0]->seq_step_tasks_onwer)){
				 $ownerid  = $GetSequenceOrder[0]->seq_step_tasks_onwer;
			}
		}else{
			$ownerid = $Taskownerid;
		}
		
		
		$onwer 				= $this->Get_data_Owner_cronjob($ownerid);
		
		
		$userinfo			= $this->Get_data_affilit_cronjob($userid);
		
		$messeges			= $this->GetSequenceDate_step($seq_type_id);
		
		$mailbody			= $this->str_rwplace($messeges,$userinfo);
		
		$address_useremail	= $onwer[0]->user_email;
		
		$seq_step_subject	= $messeges[0]->seq_step_subject;
		
		$kb_smtp_settings	= $wpdb->prefix."kb_smtp_settings";
		
		$server				= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST']:'Test server';
		$smtp 				= $wpdb->get_row( "SELECT * FROM $kb_smtp_settings ORDER BY smtp_id DESC LIMIT 1");
		
		$from_mail 			= isset($smtp->from_mail) 		? $smtp->from_mail 		 : '';
		$from_name 			= isset($smtp->from_name) 		? $smtp->from_name 		 : '';
		$smtp_host 			= isset($smtp->smtp_host) 		? $smtp->smtp_host 		 : '';
		$smtp_encription 	= isset($smtp->smtp_encription) ? $smtp->smtp_encription : '';
		$smtp_port 			= isset($smtp->smtp_port) 		? $smtp->smtp_port 		 : '';
		$smtp_auth 			= isset($smtp->smtp_auth) 		? $smtp->smtp_auth 		 : '';
		$smtp_user 			= isset($smtp->smtp_user) 		? $smtp->smtp_user 		 : '';
		$smtp_pass 			= isset($smtp->smtp_pass) 		? $smtp->smtp_pass 		 : '';
		
		
		$mail             	= new PHPMailer();
		$mail->Mailer 		= 'smtp';//
		$mail->IsSMTP(); 	
		$mail->SMTPAuth   	= $smtp_auth;   
		$mail->Host       	= $smtp_host;
		$mail->Port       	= $smtp_port;
		$mail->SMTPSecure 	= $smtp_encription ;
		//$mail->SMTPDebug 	= 1;//
		$mail->IsHTML(true); //
		$mail->SingleTo   	= true; // if you want to send a same email to multiple users. multiple emails will be sent one-by-one.
		$mail->Username   	= $smtp_user; 
		$mail->Password  	= $smtp_pass; 	
		
		$from             	= $from_mail;  
		$from_name        	= $from_name;	
		
		if($mail->Username==''||$mail->Password=='')
		{
			echo 'Smtp Username Password not set';
			return false;
		}
		
		//$mail->SetFrom($from ,$from_name);	
		
		$mail->SetFrom($from, $from_name);
		//$mail->From         = $FromMailId;
		//$mail->FromName     = $from_name;
		$mail->AddReplyTo($from, $from_name);
		$mail->AddAddress($address_useremail);
		$name    		= $from_name;
		$mail->Subject 	= $seq_step_subject;
		$mailbody		= nl2br("$mailbody");
		$message 		= $mailbody;
		$mail->Body 	= $message;
		if(!$mail->Send()){	 
			$res 		= "<p style='color:red;text-align:center;margin-top:28px;
						text-shadow:1px 0px 11px red;'>Mailer Error:".$mail->ErrorInfo."</p>";		
			return false;
		}
		else
		{	
			$res 		= "<h3 style='color:green;text-align: center;
						margin-top: 28px;text-shadow: 1px 0px 11px green;'>Message sent!</h3>";	
			return true;//
		}	
	}
	
	// ****************
	// User Mail function not dor task owner. 
	// ****************/
	public function Email_Sequence_send($data='')
	{	
		date_default_timezone_set('asia/calcutta');
		global $post, $wpdb,$server ;
		require_once('PHPMailer/class.phpmailer.php');
		require_once('PHPMailer/class.smtp.php');
		
		$userid				= $data['cron_aff_user_id'];
		$seq_type_id		= $data['cron_seq_type_id'];
		
		$userinfo			= $this->Get_data_affilit_cronjob($userid);
		$messeges			= $this->GetSequenceDate_step($seq_type_id);
		
		$mailbody			= $this->str_rwplace($messeges,$userinfo);
		
		//$onwer 				= $this->Get_data_Owner_cronjob($messeges);
		$FromMailDetails = $this->GetFromMailIdForMessage($messeges[0]->seq_step_email_email);
		
		$FromMailId = $FromMailDetails['FromMail'];
		$FromName 	= $messeges[0]->seq_step_email_send_name;
		
		$address_useremail	= $userinfo[0]->af_email;
		$seq_step_subject	= $messeges[0]->seq_step_subject;
		
		$seq_step_subject = str_replace("&#039;","'",$seq_step_subject);
		
		$kb_smtp_settings	= $wpdb->prefix."kb_smtp_settings";
		
		$server				= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST']:'Test server';
		
		$smtpOb = $wpdb->get_row( "SELECT * FROM $kb_smtp_settings ORDER BY smtp_id DESC LIMIT 1");
		
		$from_mail 			= isset($smtpOb->from_mail) 		? $smtpOb->from_mail 		 : '';
		$from_name 			= isset($smtpOb->from_name) 		? $smtpOb->from_name 		 : '';
		$smtp_host 			= isset($smtpOb->smtp_host) 		? $smtpOb->smtp_host 		 : '';
		$smtp_encription 	= isset($smtpOb->smtp_encription) 	? $smtpOb->smtp_encription	 : '';
		$smtp_port 			= isset($smtpOb->smtp_port) 		? $smtpOb->smtp_port 		 : '';
		$smtp_auth 			= isset($smtpOb->smtp_auth) 		? $smtpOb->smtp_auth 		 : '';
		$smtp_user 			= isset($smtpOb->smtp_user) 		? $smtpOb->smtp_user 		 : '';
		$smtp_pass 			= isset($smtpOb->smtp_pass) 		? $smtpOb->smtp_pass 		 : '';
		
		$mail             	= new PHPMailer();
		$mail->Mailer 		= 'smtp';//
		
		$mail->IsSMTP(); 	
		$mail->SMTPAuth   	= $smtp_auth;   
		$mail->Host       	= $smtp_host;
		$mail->Port       	= $smtp_port;
		$mail->SMTPSecure 	= $smtp_encription ;
		//$mail->SMTPDebug 	= 1;//
		$mail->IsHTML(true); //
		//$mail->SingleTo   	= true; // if you want to send a same email to multiple users. multiple emails will be sent one-by-one.
		$mail->Username   	= $smtp_user; 
		$mail->Password  	= $smtp_pass; 	
		
		$from_name        	= $from_name;
			
		if($mail->Username==''||$mail->Password=='')
		{
			echo 'Smtp Username Password not set';
			return false;
		}
		
		
		
		$mail->SetFrom($FromMailId);
		//$mail->From         = $FromMailId;
		$mail->FromName     = $FromName;	
		$mail->AddReplyTo($FromMailId, $FromName);
		$mail->AddAddress($address_useremail);
		//$mail->AddAddress('amin9770534045@gmail.com');
		$name    		= $from_name;
		$mail->Subject 	= "$seq_step_subject";
		$mailbody		=nl2br("$mailbody");
		$message 		= $mailbody;
		$mail->Body 	= $message;
		
		if(!$mail->Send()){	 
			$res 		= "<p style='color:red;text-align:center;margin-top:28px;
						text-shadow:1px 0px 11px red;'>Mailer Error:".$mail->ErrorInfo."</p>";		
			return false;
		}
		else
		{	
			$res 		= "<h3 style='color:green;text-align: center;
						margin-top: 28px;text-shadow: 1px 0px 11px green;'>Message sent!</h3>";	
			return true;//
		}		
	}
	
	 function write_file($timestamp) {
		 global $post, $wpdb,$server ;
		 $SQL = 'INSERT INTO `cron_log`(`id`, `time`) VALUES ([value-1],'.date("d/m/y : H:i:s", $timestamp).')';
		 $wpdb->insert("cron_log", array( 'time'  => date("d/m/y : H:i:s", $timestamp)));
		
	  	return true;
	 }
	
	
	
	
	/*****************
	# Owner User Data information Cron job Sequence 
	******************/
	function Get_data_Owner_cronjob($userid='')
	{		
			global $post, $wpdb;
			$id 			= $userid;
			$users 	= $wpdb->prefix."users";
			$Sql			= "select * from $users	where ID=".$id;
			$result 		= $wpdb->get_results($Sql);
			return $result;
	}
	
	public function UpdateStepRunCount_AndLastRunTime($stepID){
		global $post, $wpdb;
		$kb_sequence_step 	= $wpdb->prefix."kb_sequence_step";
		$time =  current_time('mysql', 1);
					  
		$wpdb->query("UPDATE $kb_sequence_step SET seq_run_count = seq_run_count+1, seq_last_run = '".$time."' WHERE seq_step_id = $stepID");
	}
}

?>