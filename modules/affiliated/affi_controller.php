<?php
interface model 
{	
	  	public function foo($x);	
}

require_once('aff_model.php');
/*
 * Controler class
 */
class AffiliatedControler {
	protected $model=null;
 	
	  public function __construct(model $fc){
		  $this->model = $fc;
		  
	  }
	  
	/*
	* Add Affiliated 
	*/	
	public function InsertAfiliated($post){
	  $res = $this->model->InsertAfiliated($post);
	  return $res;
	}
	  
	/*
	* Update Affiliated
	*/
	public function UpdateAfiliated($post){
	   $res = $this->model->UpdateAfiliated($post);
	   return $res;
	}
	/*
	* Affiliated get by id
	* @ array 
	*/
	public function GetAffiliData($id){
	  $res = $this->model->GetAffiliData($id);
	  return $res;
	}
	/*
	* Delete Affiliate
	*/
	public function DeleteAffiliates($id){
	   $res = $this->model->DeleteAffiliates($id);
	   return $res;
	}
	
	/*
	* Add Karatbars 
	*/	
	public function InsertKaratbarsed($post){
	  $res = $this->model->InsertKaratbarsed($post);
	  return $res;
	}
	  
	/*
	* Update Karatbars
	*/
	public function UpdateKaratbarsed($post){
	   $res = $this->model->UpdateKaratbarsed($post);
	   return $res;
	}
	
	
	/*
	* Delete Affiliate
	*/
	public function DeleteKaratbars($id){
	   $res = $this->model->DeleteKaratbars($id);
	   return $res;
	}
	
	
	/*
	* Affiliated get by id
	* @ array 
	*/
	public function GetKaratbarsData($id){
	  $res = $this->model->GetKaratbarsData($id);
	  return $res;
	}
	
	/*
	 * Get Redirection URL
	 */
	public function GetRedirectionURl(){
		$res = $this->model->GetRedirectionURl();
		$redirection_url    = isset($res->redirection_url)? $res->redirection_url : '';
	  	return $redirection_url;
	}
}

	/* 
	* Controller object 
	*/ 
	$implement  = new Affimodel();
	$AffiObject = new AffiliatedControler($implement);

?>