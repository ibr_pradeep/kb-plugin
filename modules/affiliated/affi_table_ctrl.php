<?php
if( ! class_exists( 'WP_List_Table' ) ) {
   require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
require_once('aff_model.php');

/**
 * Create a new table class that will extend the WP_List_Table
 */
class Affiliated extends WP_List_Table
{
	protected $model=null;
 	
	public function __construct(model $fc){
		$this->model = $fc;
		 global $status, $page;
	
	}
	
		 
	 
    /**
     * Prepare the items for the table to process
     * @return Void
     */
    public function prepare_items()
    {	 //echo  $users = $wpdb->prefix.'users'; die;
		$this->process_bulk_action();			
        $columns 	 = $this->get_columns();
        $hidden 	 = $this->get_hidden_columns();
        $sortable 	 = $this->get_sortable_columns();
 
        $data 		 = $this->table_data();
		//echo"<pre>"; print_r($data); die;
        usort( $data, array( &$this, 'sort_data' ) );
 
        $perPage = 15;
        $currentPage = $this->get_pagenum();
        $totalItems  = count($data);
 
        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );
 
        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);
 
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
		
	
    }
	
    public function get_columns()
    {
        $columns = array(
			'cb'        		=> '<input type="checkbox" />',
           	//'af_id'         	=> 'ID',
            'af_fname'       	=> 'Name',
            'af_email' 			=> 'Email',
            'kb_username'       => 'KB User name',
            'kb_password'    	=> 'KB Password',
            'kb_sponsor'      	=> 'Sponsor',
			'kb_package'    	=> 'Has Package',
			'user_login'      	=> 'Signed up by',
			'af_add_date'      	=> 'Date Added'
			//'af_add_karab'     	=> 'Action'
        );
 		
        return $columns;
    }
 
    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }
 
    /**
     * Define the sortable columns
     *
     * @return Array
     */
	    
    public function get_sortable_columns()
    {	$short =array(
				//'af_id' 		=> array('af_id', true),
				'af_email' 		=> array('af_email', false),
				'kb_username' 	=> array('kb_username', false),
				'kb_password' 	=> array('kb_password', false),
				'kb_sponsor' 	=> array('kb_sponsor', false),
				'kb_package'	=> array('kb_package', false),
				'user_login' 	=> array('kb_signup_by', false),
				'af_add_date' 	=> array('af_add_date', false),
				//'af_add_karab' 	=> array('karatbars', false)				
				); 
        return $short;
    }
 
    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data()
    {
		$affilites = $this->model->GetData();
		$affilites = (array)$affilites;
		$data = array();
		
		foreach($affilites as $affi){
			if($affi->af_dob){
				$affi->af_add_date = date('m/d/Y' ,strtotime($affi->af_add_date));
			}
			if($affi->af_add_date){
				$affi->af_add_date = date('m/d/Y' ,strtotime($affi->af_add_date));
			}
			$data[] = (array)$affi;
				
		}
		
		return $data;
	}
 
    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
			
            //case 'af_id':
            case 'af_fname':
            case 'af_email':
            case 'kb_username':
            case 'kb_password':
            case 'kb_sponsor':
			case 'kb_package':
			case 'user_login':
			case 'af_add_date':
			case 'af_add_karab':
                return $item[ $column_name ];
 
            default:
                return print_r( $item, true ) ;
        }
    }

    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'af_id';
        $order = 'asc';
 
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }
 
        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }
 
 
        $result = strnatcmp( $a[$orderby], $b[$orderby] );
 
        if($order === 'asc')
        {
            return $result;
        }
 
        return -$result;
    }
	 
	 
	public function get_bulk_actions() {
		  $actions = array(
			'delete'    => 'Delete'
		  );

		  return $actions;
	}
	public function column_cb($item) { //print_r($item); die;
		
        return sprintf(
            '<input type="checkbox" name="book[]" value="%s" />', $item['af_id']
        );    
    }
	
	public function column_af_fname($item) 
	{ //print_r($item); die;
		//$del ="confirm('Are you sure you want to delete this row?')"; onclick="'.$del.'"
		$CurrentUser_ID = get_current_user_id();
		$permission = $this->GetcurrentUserRol($CurrentUser_ID);
	
		if($permission['administrator']) {
			  $actions = array(
						'edit'      => sprintf('<a href="admin.php?page=kb-plugin&edit='.$item['af_id'].'">Edit</a>',$_REQUEST['page'],'edit',$item['af_id']),
						'delete'    => sprintf('<a href="?page=kb-plugin&delete='.$item['af_id'].'" id="de$del"  >Delete</a>',$_REQUEST['page'],'delete',$item['af_id']),
					);
			
			  return sprintf('%1$s %2$s', $item['af_fname'], $this->row_actions($actions) );
		  }
		  else if($permission['contributor']){
			  
			   $actions = array(
						'edit'      => sprintf('<a href="admin.php?page=kb-plugin&edit='.$item['af_id'].'">Edit</a>',$_REQUEST['page'],'edit',$item['af_id']),						
					);
			
			  return sprintf('%1$s %2$s', $item['af_fname'], $this->row_actions($actions) );
		  }
		  else
		  {
			  $actions = array(
					'Edit not allowed'      => sprintf('')
					//'delete'    => sprintf('<a href="?page=kb-plugin&delete='.$item['af_id'].'" id="de$del"  >Delete</a>',$_REQUEST['page'],'delete',$item['af_id']),
				);
			  return sprintf('%1$s %2$s', $item['af_fname'], $this->row_actions($actions) );
			  return false;  
		  }
	}
	
	public function GetcurrentUserRol($user_ID){
		 global $post,$wpdb;
		 $Sql="select meta_value from ".$wpdb->prefix."usermeta where meta_key = 'wp_capabilities' AND user_id = ".$user_ID; 
		  $Capablities = $wpdb->get_results($Sql);
		  $serialize = $Capablities[0]->meta_value;
		  $unserialize = array();
		  $unserialize = unserialize($serialize);
		  return $unserialize;
	}
	
	public function column_af_add_karab($item) 
	{ 
		return sprintf('<a href="admin.php?page=kb-plugin&edit='.$item['af_id'].'">Edit Karatbars</a>',$item['af_id']	);
		
	}

	public static function delete_customer( $id ) {
	  global $wpdb;
	  $kb_affiliated= $wpdb->prefix."kb_affiliated";
	  $wpdb->delete(
		"kb_affiliated",array('af_id' => $id )
		 );
	}
	//Detect when a bulk action is being triggered...
	
	 /**
     * [OPTIONAL] This method processes bulk actions
     * it can be outside of class
     * it can not use wp_redirect coz there is output already
     * in this example we are processing delete action
     * message about successful deletion will be shown on page in next part
     */
    public function process_bulk_action()
    {  // print_r($_REQUEST);//die;
        global $post , $wpdb;
		$kb_affiliated= $wpdb->prefix."kb_affiliated";
        if ('delete' === $this->current_action())
		{
            $ids_all = isset($_REQUEST['book']) ? $_REQUEST['book'] : array();
			foreach($ids_all as $ids)
			{
					if (!empty($ids))
					{
						 $res = $wpdb->delete("$kb_affiliated", array('af_id' => $ids));
						// $res = $wpdb->delete('kb_info', array('kb_af_id' => $ids));
					}
			}
        }
		
    }
	
	}

?>