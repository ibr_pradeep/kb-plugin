<?php date_default_timezone_set('asia/calcutta');
	//function sendmail_signup($array){
	//echo"<PRE>"; print_r($array);
	global $post, $wpdb,$server ;
	$kb_smtp_settings= $wpdb->prefix."kb_smtp_settings";
	$server= isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST']:'Test server';
	$smtp = $wpdb->get_row( "SELECT * FROM $kb_smtp_settings ORDER BY smtp_id DESC LIMIT 1");
	$from_mail 			= isset($smtp->from_mail) 		? $smtp->from_mail 		 : '';
	$from_name 			= isset($smtp->from_name) 		? $smtp->from_name 		 : '';
	$smtp_host 			= isset($smtp->smtp_host) 		? $smtp->smtp_host 		 : '';
	$smtp_encription 	= isset($smtp->smtp_encription) ? $smtp->smtp_encription : '';
	$smtp_port 			= isset($smtp->smtp_port) 		? $smtp->smtp_port 		 : '';
	$smtp_auth 			= isset($smtp->smtp_auth) 		? $smtp->smtp_auth 		 : '';
	$smtp_user 			= isset($smtp->smtp_user) 		? $smtp->smtp_user 		 : '';
	$smtp_pass 			= isset($smtp->smtp_pass) 		? $smtp->smtp_pass 		 : '';

	require_once('PHPMailer/class.phpmailer.php');
	require_once('PHPMailer/class.smtp.php');
	$mail             = new PHPMailer();
	$mail->Mailer 		= 'smtp';//
	$mail->IsSMTP(); 	
	$mail->SMTPAuth   = $smtp_auth;   
	$mail->Host       = $smtp_host;
	$mail->Port       = $smtp_port;
	$mail->SMTPSecure = $smtp_encription ;
	$mail->SMTPDebug 	= 1;//
	$mail->IsHTML(true); //
	$mail->SingleTo 	= true; // if you want to send a same email to multiple users. multiple emails will be sent one-by-one.
	$mail->Username   = $smtp_user; 
	$mail->Password   = $smtp_pass; 	
	$from             = $from_mail;  
	$from_name        = $from_name;	
	if($mail->Username==''||$mail->Password=='')
	{
		//echo 'Smtp Username Password not set';
		return ;	
	}
	

	$mail->SetFrom($from ,$from_name);	
	$mail->AddReplyTo($from, $from_name);	
	$address = $from;///$postdata['e_mail'];
	$name    = $from_name;//$postdata['f_name']." ".$postdata['l_name'];
	$mail->Subject = "SMTP SETTING TEST";
	//$mail->MsgHTML('kb plugin');
	
 	$mail->AddAddress($address);

	$mailbody=nl2br("SMTP SETTING
		Dear $name,
		TEST MAIL FOR SMTP
		
		");
	$message =$mailbody;
	$mail->Body = $message;
	if(!$mail->Send()){	
		$res = "<p style='color:red;text-align: center;margin-top: 28px;text-shadow: 1px 0px 11px red;'>Mailer Error: " . $mail->ErrorInfo."</p>";	
	}
	//return true;//
	echo $res;
	
//}

?>