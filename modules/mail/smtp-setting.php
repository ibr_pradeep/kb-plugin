<?php
include_once('smtp-config.php');
global $post, $wpdb;

$kb_setting = $wpdb->prefix."kb_setting";
if(isset($_POST['add-sponsor'])){
	
	if($_POST['sp_id'] == '')	{
		$wpdb->insert("$kb_setting",
			array(	"id" 	  			 => '',
					"kb_default_sponsor" => $_POST['kb_default_sponsor'],
					"redirection_url" 	 => $_POST['redirection_url']
					));
	}else{
		$wpdb->update("$kb_setting",
			array("kb_default_sponsor"  => $_POST['kb_default_sponsor'],
				  "redirection_url" 	=> $_POST['redirection_url']),
			array("id" 	  	 			=> $_POST['sp_id']));
		
	}
}

$Sponsor 	= $wpdb->get_row( "SELECT * FROM $kb_setting ORDER BY id DESC LIMIT 1");
$kb_default_sponsor = isset($Sponsor->kb_default_sponsor)? $Sponsor->kb_default_sponsor : '';
$redirection_url    = isset($Sponsor->redirection_url)? $Sponsor->redirection_url : '';
			$sp_ID  = isset($Sponsor->id)? $Sponsor->id : '';


$kb_smtp_settings= $wpdb->prefix."kb_smtp_settings";
if(isset($_POST['add-smtp'])) {
	if($_POST['smtp_id']){
	$wpdb->update("$kb_smtp_settings",
			array(	"from_mail" 	  => $_POST['from_mail'],
					"from_name" 	  => $_POST['from_name'],
					"smtp_host" 	  => $_POST['smtp_host'],
					"smtp_encription" => $_POST['encription'],
					"smtp_port" 	  => $_POST['port'],
					"smtp_auth" 	  => $_POST['smtp_auth'],
					"smtp_user" 	  => $_POST['smtp_user'],
					"smtp_pass" 	  => $_POST['smtp_pass']
					),
			array("smtp_id" 	  => $_POST['smtp_id']));
	
	}else {
	$wpdb->insert("$kb_smtp_settings",
			array(	"from_mail" 	  => $_POST['from_mail'],
					"from_name" 	  => $_POST['from_name'],
					"smtp_host" 	  => $_POST['smtp_host'],
					"smtp_encription" => $_POST['encription'],
					"smtp_port" 	  => $_POST['port'],
					"smtp_auth" 	  => $_POST['smtp_auth'],
					"smtp_user" 	  => $_POST['smtp_user'],
					"smtp_pass" 	  => $_POST['smtp_pass']
					));
	}
}

$smtp = $wpdb->get_row( "SELECT * FROM $kb_smtp_settings ORDER BY smtp_id DESC LIMIT 1");
//echo"<pre>"; print_r($smtp); //die;
	$smtp_id 			= isset($smtp->smtp_id) 		? $smtp->smtp_id 		 : '';
	$from_mail 			= isset($smtp->from_mail) 		? $smtp->from_mail 		 : '';
	$from_name 			= isset($smtp->from_name) 		? $smtp->from_name 		 : '';
	$smtp_host 			= isset($smtp->smtp_host) 		? $smtp->smtp_host 		 : '';
	$smtp_encription 	= isset($smtp->smtp_encription) ? $smtp->smtp_encription : '';
	$smtp_port 			= isset($smtp->smtp_port) 		? $smtp->smtp_port 		 : '';
	$smtp_auth 			= isset($smtp->smtp_auth) 		? $smtp->smtp_auth 		 : '';
	$smtp_user 			= isset($smtp->smtp_user) 		? $smtp->smtp_user 		 : '';
	$smtp_pass 			= isset($smtp->smtp_pass) 		? $smtp->smtp_pass 		 : '';


?>
<link rel="stylesheet" href="../wp-content/plugins/kb-plugin/css/style.css">
<div class="wrap">
<h2>Email Server Settings</h2>
<div class="add-affi well span7 smtp">
	<form method="post" action="#" >
    <input type="hidden" name="smtp_id" value="<?php echo $smtp_id ?>"/>
    	<div class="fonm-field">
        	<label> From Mail Address</label>
            <input type="text" name="from_mail" required value="<?php echo $from_mail ?>">
            
        </div>
        <div class="fonm-field">
        	<label>From Name</label>
            <input type="text" name="from_name"  required value="<?php echo $from_name ?>">
            
        </div>
        <div class="fonm-field">
        	<label>SMTP Host</label>
            <input type="text" name="smtp_host"  required value="<?php echo $smtp_host ?>">
        </div>
        <div class="fonm-field">
        	<label>Type Of Encription</label>
            <input type="radio" name="encription" value="none" <?php echo ($smtp_encription == 'none') ? 'checked' : ''?>>None
            <input type="radio" name="encription" value="ssl"  <?php echo ($smtp_encription == 'ssl')  ? 'checked' : ''?>>SSL
            <input type="radio" name="encription" value="tls"  <?php echo ($smtp_encription == 'tls')  ? 'checked' : ''?>>TLS
            
        </div>

        <div class="fonm-field">
        	<label>SMTP Port</label>
            <input type="text" name="port" required value="<?php echo $smtp_port ?>">
            
        </div>
        <div class="fonm-field">
        	<label>SMTP Authentication</label>
            <input type="radio" name="smtp_auth" value="false"  <?php echo ($smtp_auth == 'false')  ? 'checked' : ''?>>No 
            <input type="radio" name="smtp_auth" value="true" 	<?php echo ($smtp_auth == 'true') 	? 'checked' : ''?>>Yes            
            
        </div>
        <div class="fonm-field">
        	<label>User Name</label>
            <input type="text" name="smtp_user" required value="<?php echo $smtp_user ?>">
            
        </div>
        <div class="fonm-field">
        	<label>Password</label>
            <input type="password" name="smtp_pass" required value="<?php echo $smtp_pass ?>">
        </div>
        <div class="fonm-field">
        	<input type="submit" value="Save" name="add-smtp" class="button button-primary">
         </div>
    </form>
</div>
<div class="add-affi well span7 ">
<h2>Default Settings</h2>
<form method="post" action="#" >
 		<div class="fonm-field">
        	<label style="font-size:16px"><b>Sponsor</b></label>
            <input type="text" name="kb_default_sponsor" required value="<?php echo $kb_default_sponsor ?>">
            <input type="hidden" name="sp_id" required value="<?php echo $sp_ID ?>">
        </div>
        <div class="fonm-field">
            <label style="font-size:16px"><b>Redirection URL</b></label>
            <input type="text" name="redirection_url" required value="<?php echo $redirection_url; ?>">
        </div>
            <input type="submit" value="Save" name="add-sponsor" class="button button-primary">
        
       
   </form>
</div>

</div> <!---wrap-->
