<link rel="stylesheet" href="../wp-content/plugins/kb-plugin/css/style.css">
<?php require_once('messages_controller.php');

if(isset($_POST['add-messages']))
	{
			if (isset($_POST['messages_id'])) 
			{
				$tr = $MessagesObject->UpdateMessagesed($_POST);
				 ?>
                     <script type="text/javascript">
						window.location = '<?php get_permalink(); ?>admin.php?page=messages';
						</script>
                     <?php 
			}
			else if ($MessagesObject->InsertMessagesed($_POST))
			{		
					
					 ?>
                     <script type="text/javascript">
						window.location = '<?php get_permalink(); ?>admin.php?page=messages';
						</script>
                     <?php 
   
				
					//echo'<script  type="text/javascript">window.location = "'.get_permalink().'admin.php?page=messages'	;
			}
	}
	if(isset($_GET['edit']))
	{
		$MessagesData 			= $MessagesObject->GetMessagesData($_GET['edit']);
		//echo"<PRE>"; print_r($MessagesData);
		$messages_id 				= isset($MessagesData[0]->messages_id) 				?  $MessagesData[0]->messages_id 			: ''; 
		$messages_name 				= isset($MessagesData[0]->messages_name) 			?  $MessagesData[0]->messages_name 			: ''; 
		$messages_subject 			= isset($MessagesData[0]->messages_subject) 		?  $MessagesData[0]->messages_subject		: ''; 
		$messages_SendFromEmail 	= isset($MessagesData[0]->messages_SendFromEmail) 	?  $MessagesData[0]->messages_SendFromEmail	: ''; 
		$messages_SendFromName 		= isset($MessagesData[0]->messages_SendFromName)  	?  $MessagesData[0]->messages_SendFromName	: ''; 
		$messages_ReplayToEmail 	= isset($MessagesData[0]->messages_ReplayToEmail) 	?  $MessagesData[0]->messages_ReplayToEmail	: ''; 
		$messages_type 				= isset($MessagesData[0]->messages_type) 			?  $MessagesData[0]->messages_type 	 		: ''; 
		$messages_sent 				= isset($MessagesData[0]->messages_sent) 			?  $MessagesData[0]->messages_sent   		: '';
		$messages_content 			= isset($MessagesData[0]->messages_content) 		?  $MessagesData[0]->messages_content		: '';
		$messages_date 				= isset($MessagesData[0]->messages_date) 			?  $MessagesData[0]->messages_date 	 		: '';
		$messages_date_add			= isset($MessagesData[0]->messages_date_add)		?  $MessagesData[0]->messages_date_add 		: ''; 
		$messages_dueDate			= isset($MessagesData[0]->messages_dueDate)			?  $MessagesData[0]->messages_dueDate 		: ''; 
			
	}
	
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
	//$current = get_user();
	$current_user = wp_get_current_user();
	$get_users = get_users();

	$Messagesed_subject = $MessagesObject->GetData();
	//echo "<PRE>";print_r($get_users);
?> <?php //echo $messages_type;?>
<script>
jQuery(document).ready(function() {
    jQuery('#MyDate').datepicker({
        dateFormat : 'yy-mm-dd',
		yearRange:"-100:+0"
    });
		//------ADD NEW USER NAW JQUERY-------------------------------------------------------------
		jQuery('#messages_SendFromName').change(function()
		{
		   jQuery(this).val() == "" ? jQuery('#addNew').show() :jQuery('#addNew').hide() ;
		});
		if("addNew"=="<?php //echo $messages_SendFromName;?>")
		{
			jQuery('#addNew').show();
	    }	
		else
		{
			jQuery('#addNew').hide();
		}
		//------ADD NEW Email Replay-------------------------------------------------------------
		jQuery('#messages_ReplayToEmail').change(function()
		{
		   jQuery(this).val() == "" ? jQuery('#addNewuser_email').show() :jQuery('#addNewuser_email').hide() ;
		});
		if("addNewuser_email"=="<?php //echo isset($messages_ReplayToEmail) ?$messages_ReplayToEmai:'';?>")
		{
			jQuery('#addNewuser_email').show();
	    }	
		else
		{
			jQuery('#addNewuser_email').hide();
		}
		jQuery('#messages_type').change(function()
		{
		  // jQuery(this).val() == "Email" ? jQuery('#EmailSet').show() :jQuery('#TaskSet').hide() ;
		   if(jQuery(this).val()=="Email")
			{
				jQuery('#EmailSet').show();
			}	
			else
			{
				jQuery('#EmailSet').hide();
			}
			if(jQuery(this).val()=="Task")
			{
				jQuery('#TaskSet').show();
			}	
			else
			{
				jQuery('#TaskSet').hide();
			}
		});
});
</script>
<div class="wrap">
<h2>
<?php  if(isset($messages_id))
{ echo 'Edit Message';} 
else
{echo 'New Message';} ?>
</h2>
    <div class="row">
        <div class="span4">
        <div class="add-affi well span7">
            <form method="post" action="#">
               <div class="fonm-field">
                    <label>Message Type</label>
                    <select name="messages_type" style="width:15%;" id="messages_type">
                    	<option value=""> Select Type </option>
                    	<option value="Task"  <?php  if($messages_type=="Task") {echo"selected";} ?> >Task</option>
                        <option value="Email"  <?php if($messages_type=="Email"){echo"selected";} ?> >Email</option>
                    </select>
                    
                </div>
                <div class="fonm-field">
                    <label>Message Name </label>
                    <input type="text" name="messages_name" required value="<?php echo $messages_name ;?>">
                    
                </div>
                
                <div class="fonm-field">
                    <label> Subject</label>
                    <input type="text" name="messages_subject" required value="<?php echo $messages_subject; ?>">
                    
                </div>
 
            <?php if(isset($messages_id)) { ?>
            <input type="hidden" name="messages_id"  		 value="<?php echo $messages_id ;?>">
            <input type="hidden" name="messages_date"  		 value="<?php echo $messages_date ;?>">
            <input type="hidden" name="messages_date_add"  	 value="<?php echo $messages_date_add ;?>">
            
            <?php if(isset($messages_id)  && $messages_type=="Email"){ ?> 
            <input type="hidden" name="messages_dueDate"  	 value="<?php echo $messages_dueDate ;?>">
            
            <?php }	else { ?> 
            <input type="hidden" name="messages_SendFromName[]" value="<?php echo $messages_SendFromName ;?>">
            <input type="hidden" name="messages_ReplayToEmail[]" value="<?php echo $messages_ReplayToEmail ;?>">
            
            <?php }			
			}	 ?>
            <div id="EmailSet" <?php 
			 $show = 'style="display:block;"'; 
			 $hide = 'style="display:none;"'; 
			 if($messages_type=="Email") {echo"$show";} else{echo"$hide";} ?>>
				<div class="fonm-field">
						<label> Send From </label><?php // echo  $messages_SendFromEmail."<PRE>"; print_r($get_users);?>
						 <select name="messages_SendFromEmail">
							<option value="">Select Send From</option>
							<?php  foreach($get_users as $cont){ ?>
							<option value="<?php echo $cont->ID;?>" <?php if($messages_SendFromEmail==$cont->ID){ echo"selected";}?> ><?php  echo $cont->user_email ; ?> </option>
						 <?php	}	?>
						</select>
					</div> 
					<div class="fonm-field">
                        <label> From Name </label>
                        <select name="messages_SendFromName[]" id="messages_SendFromName">
                            <option value="">Select From Name</option>
                            <?php  foreach($Messagesed_subject as $cont){ 
							if($cont->messages_type =="Email" &&!empty($cont->messages_SendFromName)){?>
                            <option value="<?php echo $cont->messages_SendFromName;?>" <?php if($messages_SendFromName==$cont->messages_SendFromName){ echo"selected";}?> ><?php echo $cont->messages_SendFromName;?> </option>
                            <?php	} }	?>
                            <option value="">Add New Name</option>
                        </select>
                    </div>
                    <div class="fonm-field" id="addNew">
                    <label> Add New User Name</label>
                        <input type="text"  name="messages_SendFromName[]" ><br /><br />
                    </div>
					<div class="fonm-field">
						<label> Reply To Email </label>
						<select name="messages_ReplayToEmail[]" id="messages_ReplayToEmail">
							<option value="">Select Reply To Email</option>
							<?php  foreach($Messagesed_subject as $cont){ 
							if($cont->messages_type =="Email" &&!empty($cont->messages_ReplayToEmail)){?>
							<option value="<?php echo $cont->messages_ReplayToEmail;?>" <?php if($messages_ReplayToEmail==$cont->messages_ReplayToEmail){ echo"selected";}?> ><?php echo $cont->messages_ReplayToEmail;?> </option>
						 <?php	} }	?>
						<option value="" >Add New Email</option>
                    </select>
                </div>
                <div class="fonm-field" id="addNewuser_email">
                <label> Add New Reply Email</label>
                    <input type="text" name="messages_ReplayToEmail[]" >
                    <br />
                </div>
			</div>
            <div id="TaskSet" <?php 
			 $show = 'style="display:block;"'; 
			 $hide = 'style="display:none;"'; 
			 if($messages_type=="Task") {echo"$show";} else{echo"$hide";} ?>>				

					<div class="fonm-field">
						<label>Due Date</label>
						<input type="number" name="messages_dueDate" id="MyDate12"  value="<?php echo $messages_dueDate ;?>"> Days After Signup
					</div>
					<div class="fonm-field">
						<label> Task Owner </label>
						<select name="messages_SendFromEmail_task" id="messages_ReplayToEmail12">
							<option value="">Select Task Owner Email</option>
							<?php  foreach($get_users as $cont){ ?>
							<option value="<?php echo $cont->ID;?>" <?php if($messages_SendFromEmail==$cont->ID){ echo"selected";}?> ><?php echo $cont->display_name;?> </option>
						 <?php	}	?>
						</select>
					</div>
           
           </div>
             
            
                <div class="fonm-field">
                   <h3><strong>Message Template Body</strong> </h3>
                   <p>{'[First Name]', '[Last Name]', '[Email]', '[Birthday]', '[Address]', '[City]', '[State]', '[Zip Code]', '[Country]', '[Mobile Phone]', '[Username]', '[Password]', '[Sponsor]'} </p>
                   <?php 									
                    $content = $messages_content;
                    $editor_id = 'messages_content';
                    wp_editor( $content, $editor_id );
                    ?>
                </div>
                <div class="fonm-field">
                    <input type="submit" value="Save" name="add-messages" class="button button-primary">
                 </div>
            </form>
        </div>
        </div>
    </div>
</div> <!---wrap-->