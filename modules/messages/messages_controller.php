<?php
require_once('messages_model.php');

/*****************
 * Controler class
 */
class MessagesedControler 
{
	protected $model=null;
	
	public function __construct(model $fc)
	{
		$this->model = $fc;
	}
	
	/*******************************
	* Get All Messages  Subject
	*/	
	public function GetData()
	{
		$res = $this->model->GetData();
		return $res;
	}
	/*******************************
	* Add Messages  InsertTasksed
	*/	
	public function InsertMessagesed($post)
	{
		$res = $this->model->InsertMessagesed($post);
		return $res;
	}
	/*******************
	* Update Messages
	*/
	public function UpdateMessagesed($post){
	   $res = $this->model->UpdateMessagesed($post);
	   return $res;
	}
	/************************
	* Messages get by id
	* @ array 
	*/
	public function GetMessagesData($id){
	  $res = $this->model->GetMessagesData($id);
	  return $res;
	}
	/******************
	* Delete Messages
	*/
	public function DeleteMessages($id){
	   $res = $this->model->DeleteMessages($id);
	   return $res;
	}
	
	/*******************************
	* Add Messages  InsertMessagesed_task
	*/	
	public function InsertMessagesed_task($post)
	{
		$res = $this->model->InsertMessagesed_task($post);
		return $res;
	}
	/*******************
	* Update UpdateMessagesed_task
	*/
	public function UpdateMessagesed_task($post){
	   $res = $this->model->UpdateMessagesed_task($post);
	   return $res;
	}
	/************************
	* Messages Task get by id
	* @ array 
	*/
	public function GetMessages_taskData($id){
	  $res = $this->model->GetMessages_taskData($id);
	  return $res;
	}
	/******************
	* Delete DeleteMessages_task
	*/
	public function DeleteMessages_task($id){
	   $res = $this->model->DeleteMessages_task($id);
	   return $res;
	}
}
	
	/*******************
	* Controller object 
	*/
	$implement  = new Messagesmodel();
	$MessagesObject = new MessagesedControler($implement);
	/*if (isset($_POST['add-messages'])) 
	{	
		if (isset($_POST['messages_id'])) 
		{
			
			$MessagesObject->UpdateMessagesed($_POST);
			
		}

	}
*/
?>