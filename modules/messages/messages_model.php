<?php

interface model {	
	  	public function foo($x);	
}

class Messagesmodel implements model
{
	
	public function foo($x){
		return true;	
	}
	
	
	public function GetData()
	{
		global $wpdb;
		$kb_messages 	= $wpdb->prefix."kb_messages";
		$Sql="select * from $kb_messages ";  
		$result = $wpdb->get_results($Sql);
 		return $result;
		
	}
	/*********************
	* Messagesed by id
	*/
	public function GetMessagesData($id){
		global $post,$wpdb;
		$kb_messages 	= $wpdb->prefix."kb_messages";
		$Sql = "select * from $kb_messages
				where messages_id=".$id;
		$result = $wpdb->get_results($Sql);
		return $result;
	}
	
	/*******************
	* Add new  Messagesed
	* @ true 
	*/	 
	public function InsertMessagesed($postdata){
		global $wpdb;
		$messages_SendFromName1		= implode('',$postdata['messages_SendFromName']);
		$messages_SendFromName		= str_replace("","",$messages_SendFromName1);
		$messages_ReplayToEmail1	= implode('',$postdata['messages_ReplayToEmail']);
		$messages_ReplayToEmail		= str_replace(" ","",$messages_ReplayToEmail1);
		if($postdata['messages_type']=="Task")
		{
			$messages_SendFromEmail =  $postdata['messages_SendFromEmail_task'];
		}
		else
		{
			$messages_SendFromEmail =  $postdata['messages_SendFromEmail'];
		}
		$postdata['messages_content'] = str_replace("\'","’",$postdata['messages_content']);
		$postdata['messages_content'] = str_replace('\"','"',$postdata['messages_content']);
		$postContent = stripslashes(wp_filter_post_kses(addslashes($postdata['messages_content'])));
		$postSubject = stripslashes(wp_filter_post_kses(esc_html($postdata['messages_subject'])));
		
		$kb_messages 	= $wpdb->prefix."kb_messages";
		$wpdb->insert("$kb_messages", array(	
											"messages_type" 				=>  $postdata['messages_type'],
											"messages_name" 				=>  $postdata['messages_name'],
											"messages_subject" 				=>	$postSubject,
											"messages_content" 				=>  $postContent,	
											"messages_SendFromEmail" 		=>  $messages_SendFromEmail,
											"messages_SendFromName" 		=>  $messages_SendFromName,
											"messages_ReplayToEmail" 		=>  $messages_ReplayToEmail,
											"messages_dueDate" 				=>  $postdata['messages_dueDate'],
											"messages_date_add"				=> current_time( 'mysql' ) // ... and so on
											)
				);
		return true;

	}
	/*****************
	* Update Messagesed  
	*/
	public function UpdateMessagesed($postdata)
	{	
		$messages_SendFromName1		= implode('',$postdata['messages_SendFromName']);
		$messages_SendFromName		= str_replace("","",$messages_SendFromName1);
		$messages_ReplayToEmail1	= implode('',$postdata['messages_ReplayToEmail']);
		$messages_ReplayToEmail		= str_replace(" ","",$messages_ReplayToEmail1);
		if($postdata['messages_type']=="Task")
		{
			$messages_SendFromEmail =  $postdata['messages_SendFromEmail_task'];
		}
		else
		{
			$messages_SendFromEmail =  $postdata['messages_SendFromEmail'];
		}
		global $post, $wpdb;
		
		$postdata['messages_content'] = str_replace("\'","’",$postdata['messages_content']);
		$postdata['messages_content'] = str_replace('\"','"',$postdata['messages_content']);
		$postContent = stripslashes(wp_filter_post_kses(addslashes($postdata['messages_content'])));
		$postSubject = stripslashes(wp_filter_post_kses(esc_html($postdata['messages_subject'])));
		
		$kb_messages 	= $wpdb->prefix."kb_messages"; 
		$wpdb->update("$kb_messages", 
			  array(  
					  "messages_type" 				=>  $postdata['messages_type'],
					  "messages_name" 				=>  $postdata['messages_name'],
					  "messages_subject" 			=>	$postSubject,
					  "messages_content" 			=>  $postContent,	
					  "messages_SendFromEmail" 		=>  $messages_SendFromEmail,
					  "messages_SendFromName" 		=>  $messages_SendFromName,
					  "messages_ReplayToEmail" 		=>  $messages_ReplayToEmail,
					  "messages_dueDate" 			=>  $postdata['messages_dueDate'],
					  "messages_date_add" 		 	=>  $postdata['messages_date_add']
					  //"messages_date_add"	=>  current_time( 'mysql' ) // ... and so on
				  ),
				 array( 'messages_id' => $postdata['messages_id'] )
		  
		); 
	}	
	  /*****************
	  # Delete Messagesed
	  ******************/
	  public function DeleteMessages($id)
	  {
		  global $post, $wpdb;
		  $kb_messages 	= $wpdb->prefix."kb_messages";
		  $count = $wpdb->delete( "$kb_messages", array('messages_id'=>$id), $where_format = null );
		  return $count;
	  }
  
	  
}
?>