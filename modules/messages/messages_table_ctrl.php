<?php
if( ! class_exists( 'WP_List_Table' ) ) {
   require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
require_once('messages_model.php');

/**
 * Create a new table class that will extend the WP_List_Table
 */
class Messagesed extends WP_List_Table
{
	protected $model=null;
 	
	public function __construct(model $fc){
		$this->model = $fc;
		
	}
	
		 
	 
    /**
     * Prepare the items for the table to process
     * @return Void
     */
    public function prepare_items()
    {	
		$this->process_bulk_action();
        $columns 	 = $this->get_columns();
        $hidden 	 = $this->get_hidden_columns();
        $sortable 	 = $this->get_sortable_columns();
 
        $data 		 = $this->table_data();
		
        usort( $data, array( &$this, 'sort_data' ) );
 
        $perPage = 20;
        $currentPage = $this->get_pagenum();
        $totalItems  = count($data);
 
        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );
 
        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);
 
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
		
    }
 
    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
 		$columns = array(
			'cb'        		=> '<input type="checkbox" />',
            //'messages_id'       => 'Id',
			'messages_name'     => 'Messages Name',
            'messages_subject'  => 'Subject',
            'messages_date_add' => 'Date Added',
            'messages_type'     => 'Type',
            'messages_sent'    	=> 'Sent',
            );
        return $columns;
    }
 
    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }
 
    /**
     * Define the sortable columns
     *
     * @return Array
     */
	    
    public function get_sortable_columns()
    {	
		$short =array(
				//'messages_id' 		=> array('messages_id', true),
				'messages_name' 	=> array('messages_name', false),
				'messages_subject' 	=> array('messages_subject', false),
				'messages_date_add' => array('messages_date_add', false),
				'messages_type' 	=> array('messages_type', false),
				'messages_sent'		=> array('messages_sent', false)
				);		 

        return $short;
    }
 
    /**
     * Get the table data
     *
     * @return Array
     */
	 
    private function table_data()
    {
		$messages = $this->model->GetData();
		$messages = (array)$messages;
		$data = array();
		
		foreach($messages as $msg){
			if($msg->messages_date)
			{
				$msg->messages_date =	date('m/d/Y', strtotime($msg->messages_date));
			}
			if($msg->messages_date_add){
				$msg->messages_date_add =	date('m/d/Y', strtotime($msg->messages_date_add));
			}
				$data[] = (array)$msg;
			
		}
		
		return $data;
	}
 
    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
           // case 'messages_id':
            case 'messages_name':
            case 'messages_subject':
            case 'messages_date_add':
            case 'messages_type':
            case 'messages_sent':

                return $item[ $column_name ];
 
            default:
                return print_r( $item, true ) ;
        }
    }

    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'messages_id';
        $order = 'asc';
 
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }
 
        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }
 
 
        $result = strnatcmp( $a[$orderby], $b[$orderby] );
 
        if($order === 'asc')
        {
            return $result;
        }
 
        return -$result;
    }
	public function get_bulk_actions() {
		  $actions = array(
			'delete'    => 'Delete'
		  );
		  return $actions;
	}
	public function column_cb($item) { //print_r($item); die;
		
        return sprintf(
            '<input type="checkbox" name="book[]" value="%s" class="'.$item['messages_id'].'" />', $item['messages_id']
        );    
    }
	public function column_messages_name($item) {  //echo"<PRE>"; print_r($item); die;
		  $actions = array(
		'edit'      => sprintf('<a href="admin.php?page=messages&edit='.$item['messages_id'].'">Edit</a>',$_REQUEST['page'],'edit',$item['messages_id']),
		'delete'    => sprintf('<a href="?page=messages&delete='.$item['messages_id'].'">Delete</a>',$_REQUEST['page'],'delete',$item['messages_id'])
				);

		  return sprintf('%1$s %2$s', $item['messages_name'], $this->row_actions($actions) );
	}
	public static function delete_customer( $id ) {
	  global $wpdb;
	  $kb_messages 	= $wpdb->prefix."kb_messages";
	  $wpdb->delete("$kb_messages",array('messages_id' => $id )
		 );
	}
	//Detect when a bulk action is being triggered...
	
	 /**
     * [OPTIONAL] This method processes bulk actions
     * it can be outside of class
     * it can not use wp_redirect coz there is output already
     * in this example we are processing delete action
     * message about successful deletion will be shown on page in next part
     */
    public function process_bulk_action()
    {  // print_r($_REQUEST);//die;
        global $post , $wpdb;
        $kb_messages 	= $wpdb->prefix."kb_messages";

        if ('delete' === $this->current_action()) {
            $ids_all = isset($_REQUEST['book']) ? $_REQUEST['book'] : array();
           // if (is_array($ids)) $ids = implode(',', $ids);
				//echo $ids ;
			foreach($ids_all as $ids){
					if (!empty($ids)) {
					//	$affilites = $this->model->DeleteAffiliates($ids);
					//echo $sql = "DELETE FROM $table_name WHERE messages_id =".$ids;
						echo $res = $wpdb->delete("$kb_messages", array('messages_id' => $ids));
						//print_r($res);
					}
				}
        }
    }

}

?>