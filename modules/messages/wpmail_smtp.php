<?php
defined( 'ABSPATH' ) OR exit;
/**
 * Plugin Name: (WCM) PHPMailer SMTP Settings
 * Description: Enables SMTP servers, SSL/TSL authentication and SMTP settings.
 */

add_action( 'phpmailer_init', 'phpmailerSMTP' );
function phpmailerSMTP( $phpmailer )
{
  # $phpmailer->IsSMTP();
	# $phpmailer->SMTPAuth   = true;  // Authentication
	# $phpmailer->Host       = '';
	# $phpmailer->Username   = '';
	# $phpmailer->Password   = '';
	# $phpmailer->SMTPSecure = 'ssl'; // enable if required, 'tls' is another possible value
	# $phpmailer->Host       = '';    // SMTP Host
	# $phpmailer->Port       = 26;    // SMTP Port
}