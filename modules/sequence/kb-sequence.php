<?php
if(isset($_GET['email']) || isset($_GET['task']) || isset($_GET['edit'])) {
	require_once('sequence-step-add.php');
	
}else{
	wp_enqueue_script('jquery-ui-sortable');
	
	require_once('seq_controller.php');
	$MessagesSubject = $AffiObject->GetSequenceData();
	//echo "<PRE>";print_r($MessagesSubject);
?>
<script src="../wp-content/plugins/kb-plugin/js/jquery.min.js"></script>
<script src="../wp-content/plugins/kb-plugin/js/jquery.nestable.js"></script> 

<link rel="stylesheet" href="../wp-content/plugins/kb-plugin/css/style.css">   
<link rel="stylesheet" href="../wp-content/plugins/kb-plugin/css/nestable.css"> 
  <div class="wrap">
        <div id="icon-users" class="icon32"></div>
        <h2><strong>Sequence</strong></h2>
         <!--<p style="background-color: #EAEAEA;font-size: 16px;">Apply this [Kb_plugin_cron] shortcode on page and set server cron for page. </p>-->
        <div class="a-button">
        Add a step
        	<a href="admin.php?page=sequence&email" class="hide-if-no-js add-new-h2">Add Email</a>
            <a href="admin.php?page=sequence&task" class="hide-if-no-js add-new-h2">Add Task</a>
        </div>

<style>
.ui-sortable .amimov {     cursor:move; } 
.ui-sortable tr td a{ cursor:pointer; }    
.space_am{width:150px; text-align:left;  }
#sortable li { margin:5px; padding:5px; background:#fff;  border: 1px solid #DFDFDF;}
#sortable li:hover { background:#bbb; }
#sortable li:active { background:#999; }
</style>
<ul id="sortable">
		<?php $i=1;	
		
		foreach($MessagesSubject as $value)
		{
				
				$e_email_wait_day 	= isset($value->seq_step_wait) 	? $value->seq_step_wait :'';
				$e_email_delay_time = isset($value->seq_step_delaytime)? $value->seq_step_delaytime :'';
				$runonday		   = "<strong>Runs </strong>  ".$e_email_wait_day." Day After Previous <br><strong>Time</strong> " .$e_email_delay_time;
				
				$value_name 	   = isset($value->seq_step_subject) ? $value->seq_step_subject :'';
				$originalDate 	   = isset($value->seq_step_adddate) ? $value->seq_step_adddate :'';
				
				$last_Run = isset($value->seq_step_adddate) ? $value->seq_last_run :'';
				$step_RunCount = isset($value->seq_step_adddate) ? $value->seq_run_count :'';
			 	
		?>
        <li data-post-id="<?php echo $value->seq_id;?>" id="hide_<?php echo $value->seq_id;?>">
        <table  width="100%" id="delTable_<?php echo $value->seq_id;?>">
        <tr>
        <td width="5%" class='amimov priority id_<?php echo $value->seq_id;?>'><img src="<?php bloginfo( 'wpurl' ); ?>/wp-content/plugins/kb-plugin/img/move.png" alt="move" ><?php // echo $i;?></td>
        <td class='amimov' width="20%"><?php echo ($value->seq_step_type); ?></td>
        <td class='amimov' width="30%"><?php echo ($runonday); ?></td>
        <td class='amimov' width="20%"><strong>Name</strong> <?php echo ($value_name); ?></td>
        <td class='amimov' width="30%"><span><strong>Has Run :</strong> <?php echo $step_RunCount;?><BR /><strong>Last Run :</strong> <?php echo ($last_Run); ?></span></td>
        <td >
        <div style="width: 64px;"><a href="admin.php?page=sequence&edit=<?php echo $value->seq_step_id;?>&type=<?php echo $value->seq_step_type;?>" class="" ><img src="<?php bloginfo( 'wpurl' ); ?>/wp-content/plugins/kb-plugin/img/edit1.png" alt="move" style="width:26px;" /> </a>
        <button id='delete-row' style="cursor:pointer;   width: 31px;  border: none; height: 31px; background:url(<?php bloginfo( 'wpurl' ); ?>/wp-content/plugins/kb-plugin/img/dele.png)" onclick="delete_row(<?php echo $value->seq_id;?>,<?php echo $value->seq_step_id;?>)" ></button>
        </div></td>
        </tr></table>
        </li>
        <?php $i++;	
		}?>

</ul>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script> 

 $(document).ready(function() {
	 //////////////////////////
    $('#sortable').sortable({
		axis: 'y',
        update: function(event, ui) {
            $('#sortable').children().each(function(i) {
                var id = $(this).attr('data-post-id')
                    ,order = $(this).index() + 1;
			   var MyAjax = '<?php bloginfo( 'wpurl' ); ?>/wp-content/plugins/kb-plugin/modules/sequence/seq_controller_ajex.php';
                // Update order fields from posts in db
				var dataString = "id="+id+"&seq_order="+order;
				$.ajax({
				type: 'post',
				url: MyAjax,
				data: dataString,
				cache: false,
					success: function(data){
						$('#result').show();
						$('#result').html(data);
					}
							
				});
            });

        }
    }); 
	
    }); 
	///////////////////////////////////////////////////
	 function delete_row(id,seq_step_id)
	  {   // alert(seq_step_id);alert(id);
        if (confirm("Are you sure you want to delete this row?"))
        {	
			var MyAjax = '<?php bloginfo( 'wpurl' ); ?>/wp-content/plugins/kb-plugin/modules/sequence/seq_delete_ajex.php';
            //var dataString = 'id=' + id ;
			var data = "id="+id +"&seq_step_id="+seq_step_id;
			//alert(data);
            $.ajax({
                   type: 'post',
                   url: MyAjax,
                   data: data,
                   success: function()
                   {
					   $('#hide_'+id).fadeOut('slow');
                   }
			
			 });
        }
    }
	//////////////////////////////////////////////////
</script>
</div>
<?php } ?>