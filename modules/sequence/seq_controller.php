<?php
require_once('seq_model.php');

/*
 * Controler class
 */
class SeqController  {
	protected $model=null;
 	
	  public function __construct(model $fc){
		  $this->model = $fc;
		  
	  }
	/*
	* Add InsertSequence_step 
	*/	
	public function InsertSequence_step($post){
	  $res = $this->model->InsertSequence_step($post);
	  return $res;
	}
	/*
	* Update UpdateSequence_step
	*/
	public function UpdateSequence_step($post){
	   $res = $this->model->UpdateSequence_step($post);
	   return $res;
	}	  
	/*
	* Add InsertSequence_emailed 
	*/	
	public function InsertSequence_emailed($post){
	  $res = $this->model->InsertSequence_emailed($post);
	  return $res;
	}
	/*
	* ALL DATA Sequence
	*/	
	public function GetSequenceData(){
	  $res = $this->model->GetSequenceData();
	  return $res;
	}
	/************************
	* GetSequenceData_step get by id
	* @ array 
	*/
	public function GetSequenceData_step($id){
	  $res = $this->model->GetSequenceData_step($id);
	  return $res;
	} 
	/*
	* Update Affiliated
	*/
	public function UpdateSequence_emailed($post){
	   $res = $this->model->UpdateSequence_emailed($post);
	   return $res;
	}
	/*
	* GetSequence_Emailed get by Dropdown
	* @ array 
	*/
	public function GetSequence_Emailed(){
	  $res = $this->model->GetSequence_Emailed();
	  return $res;
	}
	/*
	* GetSequence_Tasksed get by Dropdown
	* @ array 
	*/
	public function GetSequence_Tasksed(){
	  $res = $this->model->GetSequence_Tasksed();
	  return $res;
	}
	/*
	* Delete DeleteSequence_emailed
	*/
	public function DeleteSequence_emailed($id){
	   $res = $this->model->DeleteSequence_emailed($id);
	   return $res;
	}
	
	/*
	* Add InsertSequence_tasked 
	*/	
	public function InsertSequence_tasked($post){
	  $res = $this->model->InsertSequence_tasked($post);
	  return $res;
	}
	  
	/*
	* Update UpdateSequence_tasked
	*/
	public function UpdateSequence_tasked($post){
	   $res = $this->model->UpdateSequence_tasked($post);
	   return $res;
	}
	
	
	/*
	* Delete DeleteSequence_tasked
	*/
	public function DeleteSequence_tasked($id){
	   $res = $this->model->DeleteSequence_tasked($id);
	   return $res;
	}
	
	
	/*
	* GetSequence_taskedData get by id
	* @ array 
	*/
	public function GetSequence_taskedData($id){
	  $res = $this->model->GetSequence_taskedData($id);
	  return $res;
	}
	
	/*
	* Mail send sequence 
	* @ array 
	*/ 
	public function usermailSend()
	{
	   $res = $this->model->usermailSend();
	  return $res;
	}
	
}

	/* 
	* Controller object 
	*/ 
	$implement  = new Seqmodel();
	$AffiObject = new SeqController($implement);

?>