<?php
interface model {	
	  	public function foo($x);	
}

class Seqmodel implements model 
{
	
	public $db;
	public function foo($x){
	  return true;	
	}
	
	/*
	* Get  Sequence Data for Data table
	*/
	public function GetSequenceData(){
	   global $post,$wpdb;
	   $kb_sequence 	= $wpdb->prefix."kb_sequence";
		$kb_sequence_step 	= $wpdb->prefix."kb_sequence_step";
	   $Sql			="select * from $kb_sequence
	   				 left JOIN $kb_sequence_step  ON $kb_sequence.seq_type_id  = $kb_sequence_step.seq_step_id
					 ORDER BY  `$kb_sequence`.`seq_order` ASC ";
	  $result = $wpdb->get_results($Sql);
	  return $result;
	}
	
	/*********************
	* Messagesed by id
	*/
	public function GetSequenceData_step($id){
		global $post,$wpdb;
		$kb_sequence_step 	= $wpdb->prefix."kb_sequence_step";
		$Sql = "select * from $kb_sequence_step
				where seq_step_id=".$id;
		$result = $wpdb->get_results($Sql);
		return $result;
	}
	/*
	* Get  Sequence Data for ALL ROW table
	*/
	public function GetSequenceDataAll(){
		global $post,$wpdb;
		$kb_sequence 	= $wpdb->prefix."kb_sequence";
		$Sql		="select * from $kb_sequence ";
		$result 	= $wpdb->get_results($Sql);
		return count($result);
	}
	/*
	* Get Affiliated Data for Data table
	*/
	public function GetData(){
	   global $post,$wpdb;
	   $kb_affiliated 	= $wpdb->prefix."kb_affiliated";
	   $Sql			="select * from $kb_affiliated ";
			
	  $result = $wpdb->get_results($Sql);
	  return $result;
	}
	
	/*
	* GetSequence_emailedData by id
	*/
	public function GetSequence_emailedData($id){
		global $post,$wpdb;
		$kb_email_step 	= $wpdb->prefix."kb_email_step";
		$Sql 		= "select * from $kb_email_step
				where e_email_id=".$id;
		$result = $wpdb->get_results($Sql);
		return $result;
	}
	/*
	* GetSequence_emailedData by kb_messages.Email
	*/
	public function GetSequence_Emailed(){
		global $post,$wpdb;
		$kb_messages 	= $wpdb->prefix."kb_messages";
		$Sql 			= "select * from $kb_messages";
		$result 		= $wpdb->get_results($Sql);
		return $result;
	}
	/*
	* Get Task to sequence by kb_tasks.SUBJECT
	*/
	public function GetSequence_Tasksed(){
		global $post,$wpdb;
		$kb_tasks 	= $wpdb->prefix."kb_tasks";
		$Sql 		= "select * from $kb_tasks";
		$result 	= $wpdb->get_results($Sql);
		return $result;
	}
	/**
	* Add new InsertSequence_step
	* @ true 
	*/	 
	public function InsertSequence_step($postdata){
	global $post, $wpdb;
		$kb_sequence_step 	= $wpdb->prefix."kb_sequence_step";
		if(!empty($postdata['seq_step_weekday']))
		{
			$e_email_day_week = implode(',',$postdata['seq_step_weekday']);
		}
		else
		{
			$e_email_day_week ='';
		}
	
		if($postdata['seq_step_Delay_Times']=="Delay")
		{	$seq_step_delaytime=$postdata['seq_step_delaytime'];}
		else
		{	$seq_step_delaytime=$postdata['seq_step_delaytimeTimes'];	}
		if($postdata['seq_step_tasks_onwer_type']=="Group")
		{	$seq_step_tasks_onwer=$postdata['seq_step_tasks_onwer'];}
		else
		{	$seq_step_tasks_onwer=$postdata['seq_step_tasks_onwerSingle'];	}
		
		$postdata['seq_step_content'] = str_replace("\'","’",$postdata['seq_step_content']);
		$postdata['seq_step_content'] = str_replace('\"','"',$postdata['seq_step_content']);
		
		$postContent = stripslashes(wp_filter_post_kses(addslashes($postdata['seq_step_content'])));
		$postSubject = stripslashes(wp_filter_post_kses(esc_html($postdata['seq_step_subject'])));
		
		$wpdb->insert("$kb_sequence_step", 
					  array("seq_step_subject" 			=>  $postSubject,
							"seq_step_wait" 			=>	$postdata['seq_step_wait'],
							"seq_step_Delay_Times" 		=>  $postdata['seq_step_Delay_Times'],
							"seq_step_delaytime" 		=>  $seq_step_delaytime,
							"seq_step_weekday" 			=>  $e_email_day_week,								   				  
							"seq_step_type" 			=>  $postdata['seq_step_type'],
							"seq_step_content"			=>  $postContent,
							"seq_step_email_email"		=>  $postdata['seq_step_email_email'],
							"seq_step_email_send_name"	=>  $postdata['seq_step_email_send_name'],
							"seq_step_email_rplay_email"=>  $postdata['seq_step_email_rplay_email'],
							"seq_step_email_email_from"	=>  $postdata['seq_step_email_email_from'],
							"seq_step_tasks_due_date"	=>  $postdata['seq_step_tasks_due_date'],
							"seq_step_tasks_onwer"		=>  $seq_step_tasks_onwer,
							"seq_step_tasks_onwer_type"	=>  $postdata['seq_step_tasks_onwer_type'],
							"seq_step_task_complete"	=>  $postdata['seq_step_task_complete'],
							"seq_step_adddate"			=>  date("Y-m-d h:i:s") //current_time( 'mysql' )
						  )
			  );
		$lastid 	= $wpdb->insert_id; 
		$seq_order	= $this->GetSequenceDataAll();
		$kb_sequence= $wpdb->prefix."kb_sequence";
		$wpdb->insert("$kb_sequence",
					array(
						"seq_type_id" 		=> $lastid,
						"seq_order" 		=>$seq_order+1,
						"seq_type" 			=>$postdata['seq_step_type'],
						"seq_adddate"	=>  current_time( 'mysql' )
					 	)
					);
	  return true;
	}
	/**
	* Update UpdateSequence_Step  
	*/
	public function UpdateSequence_step($postdata)
	{ 
		global $post, $wpdb;
		$kb_sequence_step = $wpdb->prefix."kb_sequence_step";
			if($postdata['seq_step_Delay_Times']=="Delay")
			{	$seq_step_delaytime=$postdata['seq_step_delaytime'];}
			else
			{	$seq_step_delaytime=$postdata['seq_step_delaytimeTimes'];	}
			if($postdata['seq_step_tasks_onwer_type']=="Group")
			{	$seq_step_tasks_onwer=$postdata['seq_step_tasks_onwer'];}
			else
			{	$seq_step_tasks_onwer=$postdata['seq_step_tasks_onwerSingle'];	}
		
		$e_email_day_week= isset($postdata['seq_step_weekday'])?implode(',',$postdata['seq_step_weekday']):'';
		$postdata['seq_step_content'] = str_replace("\'","’",$postdata['seq_step_content']);
	
		$postdata['seq_step_content'] = str_replace('\"','"',$postdata['seq_step_content']);
		$postContent = stripslashes(wp_filter_post_kses(addslashes($postdata['seq_step_content'])));
		$postSubject = stripslashes(wp_filter_post_kses(esc_html($postdata['seq_step_subject'])));
		
		$wpdb->update("$kb_sequence_step", 
				array(	"seq_step_subject" 			=>  $postSubject,
						"seq_step_wait" 			=>	$postdata['seq_step_wait'],
						"seq_step_Delay_Times" 		=>  $postdata['seq_step_Delay_Times'],
						"seq_step_delaytime" 		=>  $seq_step_delaytime,
						"seq_step_weekday" 			=>  $e_email_day_week,								   				  
						"seq_step_type" 			=>  $postdata['seq_step_type'],
						"seq_step_content"			=>  $postContent,
						"seq_step_email_email"		=>  $postdata['seq_step_email_email'],
						"seq_step_email_send_name"	=>  $postdata['seq_step_email_send_name'],
						"seq_step_email_rplay_email"=>  $postdata['seq_step_email_rplay_email'],
						"seq_step_email_email_from"	=>  $postdata['seq_step_email_email_from'],
						"seq_step_tasks_due_date"	=>  $postdata['seq_step_tasks_due_date'],
						"seq_step_tasks_onwer"		=>  $seq_step_tasks_onwer,
						"seq_step_tasks_onwer_type"	=>  $postdata['seq_step_tasks_onwer_type'],
						"seq_step_task_complete"	=>  $postdata['seq_step_task_complete'],
						"seq_step_adddate"			=>  current_time( 'mysql' )
					  ),
					  array( 'seq_step_id' => $postdata['seq_step_id'] )
		  
		); 
	}	

	
	  /*
	  * Delete GetSequence_taskedData
	  */
	  public function DeleteSequence_tasked($id){
		  global $post, $wpdb;
		  $kb_task_step = $wpdb->prefix."kb_task_step";
		  $count = $wpdb->delete( "$kb_task_step", array('s_tasks_id'=>$id), $where_format = null );
		  
		  return $count;
	  }
	  public function delete_af_id($postdata){
		global $post, $wpdb;
		$kb_task_step = $wpdb->prefix."kb_task_step";
		$wpdb->delete("$kb_task_step", 
				array("s_tasks_id" 		=>  $postdata['s_tasks_id']
					  )
			  );
		
		  return true;
		}
		
		
	
}
?>