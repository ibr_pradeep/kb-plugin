<link rel="stylesheet" href="../wp-content/plugins/kb-plugin/css/style.css">
<?php
	if(isset($_GET['edit']))
	{
		require_once('tasks_controller.php');
		$MessagesData 			= $Tasksed->GetTasksData($_GET['edit']);
		$tasks_id 		    = isset($MessagesData[0]->tasks_id) 		?  $MessagesData[0]->tasks_id 		: ''; 
		$tasks_subject 		= isset($MessagesData[0]->tasks_subject) 	?  $MessagesData[0]->tasks_subject 		: ''; 
		$tasks_due_date 	= isset($MessagesData[0]->tasks_due_date) 	?  $MessagesData[0]->tasks_due_date	: ''; 
		$tasks_onwer 		= isset($MessagesData[0]->tasks_onwer) 		?  $MessagesData[0]->tasks_onwer 	 	: ''; 
		$tasks_releted_to 	= isset($MessagesData[0]->tasks_releted_to) ?  $MessagesData[0]->tasks_releted_to  	: ''; 
		$tasks_status 		= isset($MessagesData[0]->tasks_status) 	?  $MessagesData[0]->tasks_status   	: '';
		$tasks_content	 	= isset($MessagesData[0]->tasks_content) 	?  $MessagesData[0]->tasks_content	: ''; 
		$tasks_Cron_id		= isset($MessagesData[0]->tasks_Cron_id) 	?  $MessagesData[0]->tasks_Cron_id	: ''; 
	}
	if(isset($_POST['add-tasks']))
	{
			require_once('tasks_controller.php');
			if (isset($_POST['tasks_id'])) 
			{	//echo"<pre>"; print_r($_POST); die; 
				$responce = $Tasksed->UpdateTasksed($_POST);
				//For Run Cron
				//print_r($responce); 
				if(strtolower($responce['status']) == 'completed'){
					$result = updateCronTaskStatus($tasks_Cron_id);
					if($result == true){
						
						include_once(PLUGIN_DIR.'modules/cron_job/cron_job.php'); 
					}
				}
				
				?>
                     <script type="text/javascript">
						window.location = '<?php get_permalink(); ?>admin.php?page=tasks';
						</script>
                     <?php 
			}
			elseif ($Tasksed->InsertTasksed($_POST))
			{	?>
                     <script type="text/javascript">
						window.location = '<?php get_permalink(); ?>admin.php?page=tasks';
						</script>
                     <?php 
			}
		
	}
	function updateCronTaskStatus($CronId){ //echo"hello";
		global $post, $wpdb;
		//print_r($CronId); die('hi');
		$wp_kb_sequence_cron_job= $wpdb->prefix."kb_sequence_cron_job";
		$wpdb->update("$wp_kb_sequence_cron_job", 
			  array(  "cron_status" 		=>  1,
			  		  "is_paused" 			=>  0 ),
			  array( 'cron_id' => $CronId)
		  
		); 
		return true;
	}
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
	$current_user = get_users();
	//$current_user = $current_user1->data;
	
	$All_aff = $Tasksed->tasks_releted_to();
	//echo "<PRE>";print_r($All_aff);

?>
<script>
jQuery(document).ready(function() {
    jQuery('#MyDate').datepicker({
        dateFormat : 'yy-mm-dd',
		//changeMonth:true,
       // changeYear:true,
		//yearRange:"-100:+0"
    });
});
</script>
<div class="wrap">
<h2> <?php  if(isset($tasks_id)){ echo 'Edit Tasks';}else{echo 'Add New Tasks';} // echo plugins_url();?> </h2>
<div class="add-affi well span7">
	<form method="post" action="#">
    <?php  if(isset($tasks_id)){ echo '<input type="hidden" name="tasks_id" value="'.$tasks_id.'">';} ?>
    	<div class="fonm-field">
        	<label>Task Subject</label>
            <input type="text" name="tasks_subject"  value="<?php echo $tasks_subject ;?>" readonly="readonly">
            
        </div>
        <div class="fonm-field">
        	<label>Due Date</label>
            <input type="hidden"  name="tasks_due_date"  value="<?php echo $tasks_due_date;?>"/>
            <input type="text"  id="MyDate1"  value="<?php echo date('m/d/Y',strtotime($tasks_due_date));?>" readonly="readonly">Days After Signup
            
        </div>
        <div class="fonm-field">
        	<label>Task Onwer</label>
            <select name="tasks_onwer" readonly="readonly">
             <?php 	foreach($current_user as $cont){ ?>
            <option value="<?php echo $cont->ID;?>"  <?php if($tasks_onwer==$cont->ID){ echo"selected";} else{echo"disabled";}?> ><?php  echo $cont->display_name;?> </option>
         <?php	}	?>
            </select>
            
        </div>
        <div class="fonm-field">
        	<label>Related To</label>
           <!-- <input type="text" name="tasks_releted_to"  value="<?php echo $tasks_releted_to ;?>">-->
           <select name="tasks_releted_to" readonly="readonly">
           		<?php   
			foreach($All_aff as $cont){ ?>
            	<option value="<?php echo $cont->af_id;?>" <?php  if($tasks_releted_to==$cont->af_id) {echo"selected";} else{echo"disabled";} ?>><?php  echo $cont->af_fname." ".$cont->af_lname;?></option>			
                <?php	}	?>
            </select>
            
        </div>

        <div class="fonm-field">
        	<label>Status</label>
            <select name="tasks_status">
            	<option value="Not Strated" <?php  if($tasks_status=="Not Strated") {echo"selected";} ?>>Not Started</option>
                <option value="In Progress" <?php  if($tasks_status=="In Progress") {echo"selected";} ?>>In Progress</option>
				<option value="Completed" <?php  if($tasks_status=="Completed") {echo"selected";} ?>>Completed</option>            
            </select>
            
        </div>
         <div class="fonm-field">
        	<label>Task Details</label><br />
            <p style="border:1px solid #CCC;min-height: 150px;padding: 12px;"> <?php echo nl2br($tasks_content) ;?></p>
            <input type="hidden" name="tasks_content"  value="<?php echo $tasks_content ;?>" />
        </div>
     
        
       <?php /*?><?php if(isset($tasks_id)){ ?>
      <div class="fonm-field">
          <label><h3><strong><span id="tasks_type">Task Details </span></strong> </h3></label><br /><br />
          <?php 									
          $content = $tasks_content;
          $editor_id = 'tasks_content';
          wp_editor( $content, $editor_id );
          ?>
      </div>
      <?php }?><?php */?>
        <div class="fonm-field">
        	<input type="submit" value="Update" name="add-tasks" class="button button-primary">
             <a href='admin.php?page=kb-plugin&edit=<?php echo $tasks_releted_to;?>&task=<?php echo $_GET['edit']; ?>' class="button button-primary">Edit Affiliate</a>
         </div>
    </form>
</div>

</div> <!---wrap-->