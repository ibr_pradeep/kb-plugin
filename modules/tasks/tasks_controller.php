<?php

require_once('tasks_model.php');

/*
 * Controler class
 */
class TasksedControler {
	protected $model=null;
 	
	public function __construct(model $fc){
		$this->model = $fc;
		
	}
	
	/*
	* Add Affiliated  InsertTasksed
	*/	
	public function InsertTasksed($post){
		$res = $this->model->InsertTasksed($post);
		return $res;
	}
	/*
	* Update UpdateTasksed
	*/
	public function UpdateTasksed($post){
	   $res = $this->model->UpdateTasksed($post);
	   return $res;
	}

	/************************
	* Messages get by id
	* @ array 
	*/
	public function GetTasksData($id){
	  $res = $this->model->GetTasksData($id);
	  return $res;
	}
	/************************
	* tasks_releted_to get by User
	* @ array 
	*/
	public function tasks_releted_to(){
	  $res = $this->model->tasks_releted_to();
	  return $res;
	}
	/******************
	* Delete DeleteTasksed
	*/
	public function DeleteTasksed($id){
	   $res = $this->model->DeleteTasksed($id);
	   return $res;
	}
	
}

/* 
 * Controller object 
 */
	 $implement  = new Tasksmodel();
	$Tasksed = new TasksedControler($implement);


?>