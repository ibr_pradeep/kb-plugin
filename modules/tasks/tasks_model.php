<?php

interface model {	
	  	public function foo($x);	
}

class Tasksmodel implements model{
	
	public function foo($x){
		return true;	
	}
	
	
	public function GetData(){
		global $wpdb;
		$users			= $wpdb->prefix."users";
		$kb_tasks		= $wpdb->prefix."kb_tasks";
		$kb_affiliated	= $wpdb->prefix."kb_affiliated";
		
		$CurrentUser_ID = get_current_user_id();
		$permission = $this->GetcurrentUserRol($CurrentUser_ID);
		if($permission['administrator']) { 
			$Sql="select * from $kb_tasks 
			left JOIN $users ON $kb_tasks.tasks_onwer = $users.ID
			left JOIN $kb_affiliated ON $kb_tasks.tasks_releted_to = $kb_affiliated.af_id";
			$result = $wpdb->get_results($Sql);
			return $result;
		}else{
			$Sql="select * from $kb_tasks 
			left JOIN $users ON $kb_tasks.tasks_onwer = $users.ID
			left JOIN $kb_affiliated ON $kb_tasks.tasks_releted_to = $kb_affiliated.af_id
			where $kb_tasks.tasks_onwer = $CurrentUser_ID";
			$result = $wpdb->get_results($Sql);
			return $result;
		}
		
	}
	
	public function GetcurrentUserRol($user_ID){
		 global $post,$wpdb;
		 $Sql="select meta_value from ".$wpdb->prefix."usermeta where meta_key = 'wp_capabilities' AND user_id = ".$user_ID; 
		  $Capablities = $wpdb->get_results($Sql);
		  $serialize = $Capablities[0]->meta_value;
		  $unserialize = array();
		  $unserialize = unserialize($serialize);
		  return $unserialize;
	}
	
	/*********************
	* Task tasks_releted_to by user
	*/
	public function tasks_releted_to(){
		global $post,$wpdb;
		$kb_affiliated = $wpdb->prefix."kb_affiliated";
		$Sql = "select * from $kb_affiliated";
		$result = $wpdb->get_results($Sql);
		return $result;
	}
	
	/*********************
	* Task by id
	*/
	public function GetTasksData($id){
		global $post,$wpdb;
		$kb_tasks= $wpdb->prefix."kb_tasks";
		$Sql = "select * from $kb_tasks
				where tasks_id=".$id;
		$result = $wpdb->get_results($Sql);
		return $result;
	}	
	
	/**
	 * Add new Task
	 * @ true 
	 */	 
	 public function InsertTasksed($post){
		global $wpdb;
		$kb_tasks= $wpdb->prefix."kb_tasks";
		$wpdb->insert("$kb_tasks", array(	"tasks_subject" 	=>  $post['tasks_subject'],
											"tasks_due_date" 	=>	$post['tasks_due_date'],
											"tasks_onwer" 		=>  $post['tasks_onwer'],
										 	"tasks_releted_to" 	=>  $post['tasks_releted_to'],									   
										   	"tasks_status" 		=>  $post['tasks_status'],
										   	"tasks_add_date"	=> current_time( 'mysql' ) // ... and so on
										)
										
					);
		return true;
	}
	/*****************
	 * Update Task  
	 *****************/
	public function UpdateTasksed($postdata)
	{
		global $post, $wpdb;
		$kb_tasks= $wpdb->prefix."kb_tasks";
		$wpdb->update("$kb_tasks", 
			  array(  "tasks_subject" 		=>  $postdata['tasks_subject'],
					  "tasks_due_date" 		=>	$postdata['tasks_due_date'],
					  "tasks_onwer" 		=>  $postdata['tasks_onwer'],
					  "tasks_releted_to" 	=>  $postdata['tasks_releted_to'],
					  "tasks_status" 		=>  $postdata['tasks_status'],
					  "tasks_content" 		=>  $postdata['tasks_content'],									   
					  "tasks_add_date" 		=>  current_time( 'mysql' ) // ... and so on
					 // "messages_date_add"	=>  current_time( 'mysql' ) // ... and so on
				  ),
					  array( 'tasks_id' => $postdata['tasks_id'] )
		  
		); 
		return array('status'=>$postdata['tasks_status']);
	}	
	  /*****************
	  # Delete Task
	  ******************/
	  public function DeleteTasksed($id)
	  {
		  global $post, $wpdb;
		  $kb_tasks= $wpdb->prefix."kb_tasks";
		  $count = $wpdb->delete( "$kb_tasks", array('tasks_id'=>$id), $where_format = null );
		  return $count;
	  }
}
?>