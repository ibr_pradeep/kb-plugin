<?php
if( ! class_exists( 'WP_List_Table' ) ) {
   require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
require_once('tasks_model.php');

/**
 * Create a new table class that will extend the WP_List_Table
 */
class Tasksed extends WP_List_Table
{
	protected $model=null;
 	
	public function __construct(model $fc){
		$this->model = $fc;
		
	}
	
		 
	 
    /**
     * Prepare the items for the table to process
     * @return Void
     */
    public function prepare_items()
    {	
		$this->process_bulk_action();
        $columns 	 = $this->get_columns();
        $hidden 	 = $this->get_hidden_columns();
        $sortable 	 = $this->get_sortable_columns();
 
        $data 		 =   $this->table_data() ;
		
		
        usort( $data, array( &$this, 'sort_data' ) );
 
        $perPage = 20;
        $currentPage = $this->get_pagenum();
        $totalItems  = count($data);
 
        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );
 
        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);
 
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
		
    }
 
    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
 		$columns = array(
            'cb'        		=> '<input type="checkbox" />',
			//'tasks_id'         		=> 'ID',
            'tasks_subject'       	=> 'Subject',
			'af_fname'    			=> 'Related To',
			'display_name'      	=> 'Onwer',
			'tasks_add_date'      	=> 'Date Added',
            'tasks_due_date' 		=> 'Due Date',
            'tasks_status'      	=> 'Status'
			
        );
        return $columns;
    }
 
    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }
 
    /**
     * Define the sortable columns
     *
     * @return Array
     */
	    
    public function get_sortable_columns()
    {	
		$short =array(
				//'tasks_id' 			=> array('tasks_id', true),
				'tasks_subject' 	=> array('tasks_subject', false),
				'af_fname' 			=> array('af_fname', false),
				'display_name' 		=> array('display_name', false),
				'tasks_due_date' 	=> array('tasks_due_date', false),
				'tasks_status'		=> array('tasks_status', false),
				'tasks_add_date' 	=> array('tasks_add_date', false)				
				);		 

        return $short;
    }
 
    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data()
    {
		$affilites = $this->model->GetData();
		$affilites = (array)$affilites;
		$data = array();
		
		foreach($affilites as $affi){
			if($affi->tasks_due_date){
				$affi->tasks_due_date = date('m/d/Y' , strtotime($affi->tasks_due_date));
			}
			if($affi->tasks_add_date){
				$affi->tasks_add_date = date('m/d/Y' , strtotime($affi->tasks_add_date));
			}
			$data[] = (array)$affi;	
		}
		
		return $data;
	}
 
    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
          //  case 'tasks_id':
            case 'tasks_subject':
            case 'tasks_due_date':
            case 'display_name':
            case 'af_fname':
            case 'tasks_status':
			case 'tasks_add_date':

                return $item[ $column_name ];
 
            default:
                return print_r( $item, true ) ;
        }
    }

    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'tasks_id';
        $order = 'asc';
 
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }
 
        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }
 
 
        $result = strnatcmp( $a[$orderby], $b[$orderby] );
 
        if($order === 'asc')
        {
            return $result;
        }
 
        return -$result;
    }
	public function get_bulk_actions() {
		  $actions = array(
			'delete'    => 'Delete'
		  );
		  return $actions;
	}
	public function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="book[]" value="%s" />', $item['tasks_id']
			
        );    
    }
	
	public function GetcurrentUserRol($user_ID){
		 global $post,$wpdb;
		 $Sql="select meta_value from ".$wpdb->prefix."usermeta where meta_key = 'wp_capabilities' AND user_id = ".$user_ID; 
		  $Capablities = $wpdb->get_results($Sql);
		  $serialize = $Capablities[0]->meta_value;
		  $unserialize = array();
		  $unserialize = unserialize($serialize);
		  return $unserialize;
	}
	
	public function column_tasks_subject($item) { //print_r($item); die;
		$CurrentUser_ID = get_current_user_id();
		$permission = $this->GetcurrentUserRol($CurrentUser_ID);
		if($permission['administrator']) {
			  $actions = array(
						'edit'      => sprintf('<a href="admin.php?page=tasks&edit='.$item['tasks_id'].'">Edit</a>',$_REQUEST['page'],'edit',$item['tasks_id']),
						'delete'    => sprintf('<a href="?page=tasks&delete='.$item['tasks_id'].'">Delete</a>',$_REQUEST['page'],'delete',$item['tasks_id']),
			
					);
		}else{
			 $actions = array(
						'edit'      => sprintf('<a href="admin.php?page=tasks&edit='.$item['tasks_id'].'">Edit</a>',$_REQUEST['page'],'edit',$item['tasks_id'])
						);
		}
		
			  return sprintf('%1$s %2$s', $item['tasks_subject'], $this->row_actions($actions) );
	}
	public static function delete_customer( $id ) {
	  global $wpdb;
	  $wpdb->delete(
		"kb_tasks",array('tasks_id' => $id )
		 );
	}
	//Detect when a bulk action is being triggered...
	
	 /**
     * [OPTIONAL] This method processes bulk actions
     * it can be outside of class
     * it can not use wp_redirect coz there is output already
     * in this example we are processing delete action
     * message about successful deletion will be shown on page in next part
     */
    public function process_bulk_action()
    {  
        global $post , $wpdb;
        $kb_tasks= $wpdb->prefix."kb_tasks";
        if ('delete' === $this->current_action())
		{
            $ids_all = isset($_REQUEST['book']) ? $_REQUEST['book'] : array();
			foreach($ids_all as $ids)
			{
					if (!empty($ids))
					{
						 $res = $wpdb->delete("$kb_tasks", array('tasks_id' => $ids));
					}
			}
        }
		
    }
}

?>